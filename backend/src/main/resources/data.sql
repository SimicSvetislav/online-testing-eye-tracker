INSERT INTO user (id, full_name, display_name, password, email, role, phone_number, index_num)
VALUES (1, "Petar Petrovic", "ppera",  
		"$2a$10$TYEJ0OSTcthhSaYJccW2l.WvFEvtSehJ0njXf6OPXB7KW0pfh0/nW", 
		"pera@gmail.com", "T",
		"061/123-45-67", null);
		
INSERT INTO user (id, full_name, display_name, password, email, role, phone_number, index_num)
VALUES (2, "Jovan Jovanovic", "zmaj",  
		"$2a$10$TYEJ0OSTcthhSaYJccW2l.WvFEvtSehJ0njXf6OPXB7KW0pfh0/nW", 
		"zmaj@gmail.com", "S",
		null, "E1234");
		
INSERT INTO question_type(id, name)
VALUES (1, "Multiple choice question");
INSERT INTO question_type(id, name)
VALUES (2, "Essay question");