package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
public @Data class EssayTestAnswerDTO extends TestAnswerDTO {

	private String answer;
	
}
