package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Student;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.StudentRepository;

@Service
public class StudentService {

	@Autowired
	private StudentRepository repo;

	public Student findById(Long id) {
		return repo.getOne(id);
	}
	
	
	
}
