package rs.ac.uns.ftn.informatics.sotis.online_testing_platform;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Region;

@SpringBootApplication
public class SotisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SotisApplication.class, args);
	}
}
