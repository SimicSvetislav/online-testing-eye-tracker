package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import java.util.List;

import lombok.Data;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.QuestionNew;

public @Data class TestDTO {
	
	private Long id;
	private String name;
	private Integer duration;
	private Boolean active;
	
	// private List<QuestionDTO> questions;
	private List<QuestionNew> questionElements;
	
	
}
