package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@EqualsAndHashCode(callSuper = false)
@DiscriminatorValue("MC")
public @Data class TestAnswerMC extends TestAnswer {

	@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<IdAnswerPair> idAnswerPairs;
	
}
