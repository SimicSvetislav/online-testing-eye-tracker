package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import java.util.List;

import lombok.Data;

public @Data class QuestionRegionSequenceDTO {

	private Long questionId;
	// private Integer visitNum;
	
	private List<List<Long>> regionSequence;
	
}
