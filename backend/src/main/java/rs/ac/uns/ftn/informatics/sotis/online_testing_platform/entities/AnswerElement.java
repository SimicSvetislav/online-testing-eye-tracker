package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@EqualsAndHashCode(callSuper = false)
@DiscriminatorValue("answer")
public @Data class AnswerElement extends BaseElement {

	private String text;
	private Boolean correct;
	
}
