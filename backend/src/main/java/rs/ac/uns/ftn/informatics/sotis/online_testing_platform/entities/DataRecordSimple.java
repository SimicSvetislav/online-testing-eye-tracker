package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities;

import lombok.Data;

public @Data class DataRecordSimple {
	
	private Double fpogx;
    private Double fpogy;
    private Double fpogs;
    private Double fpogd;
    private Integer fpogId;
    private Integer fpogv;
    private Long questionId;
    
    private Long cnt;

}
