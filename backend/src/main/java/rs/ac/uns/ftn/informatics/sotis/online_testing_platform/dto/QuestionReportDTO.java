package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import java.util.List;

import lombok.Data;

public @Data class QuestionReportDTO {

	private String text;
	private Boolean correct;
	private Long qTypeId;
	
	private List<String> answered;
	private List<String> correctAnswer;
	
}
