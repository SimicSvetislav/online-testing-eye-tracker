	package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity
public @Data class Region {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private Double x;
	private Double y;
	
	private Double width;
	private Double height;
	
	// @ManyToOne(fetch = FetchType.LAZY)
	// private Question question;
	
}
