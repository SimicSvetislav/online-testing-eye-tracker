package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import lombok.Data;

public @Data class AnswerDTO {

	private Long id;
	private String text;
	private Boolean correct;
	
}
