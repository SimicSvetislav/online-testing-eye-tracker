package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.TestingInfo;

@Repository
public interface TestingInfoRepository extends JpaRepository<TestingInfo, Long> {

	List<TestingInfo> findByStudentId(Long id);

}
