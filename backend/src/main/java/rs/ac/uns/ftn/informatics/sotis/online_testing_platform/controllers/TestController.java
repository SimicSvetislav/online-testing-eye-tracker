package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Student;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Teacher;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.User;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.StudentRepository;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.TeacherRepository;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.UserRepository;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/test")
public class TestController {
	
	@Autowired
	private UserRepository uRepo;
	
	@Autowired
	private StudentRepository sRepo;
	
	@Autowired
	private TeacherRepository tRepo;
	
	@RequestMapping(method = RequestMethod.GET, path = "/users", produces = "application/json")
	public ResponseEntity<List<User>> getUsers() {
	
		List<User> users = uRepo.findAll();
		
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/students", produces = "application/json")
	public ResponseEntity<List<Student>> getStudents() {
	
		List<Student> students = sRepo.findAll();
		
		return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/teachers", produces = "application/json")
	public ResponseEntity<List<Teacher>> getTeachers() {
	
		List<Teacher> teachers = tRepo.findAll();
		
		return new ResponseEntity<List<Teacher>>(teachers, HttpStatus.OK);
		
	}
	
	
}
