package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
public @Data class IdAnswerPair {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Long id;
	
	private Long answerId;
	private Boolean givenAnswer;
	
}
