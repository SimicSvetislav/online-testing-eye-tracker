package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import lombok.Data;

public @Data class RegionDTO {
	
	private Long id;
	
	private Double x;
	private Double y;
	
	private Double width;
	private Double height;
	
}
