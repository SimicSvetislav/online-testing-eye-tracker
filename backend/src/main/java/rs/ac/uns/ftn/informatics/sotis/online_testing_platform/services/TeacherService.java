package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.QuestionType;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Teacher;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.QuestionTypeRepository;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.TeacherRepository;

@Service
public class TeacherService {

	@Autowired
	private TeacherRepository repo;
	
	@Autowired
	private QuestionTypeRepository qtRepo;

	public Teacher findById(Long id) {
		
		Teacher t = repo.findById(id).get();
		
		return t;
	}

	public List<QuestionType> getQuestionTypes() {
		
		List<QuestionType> types = qtRepo.findAll();
		
		return types;
	}
	
	
}
