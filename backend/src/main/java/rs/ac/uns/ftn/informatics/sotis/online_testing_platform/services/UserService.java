package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.User;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.UserRepository;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository repo;
	
	public User findByDisplayName(String username) {
		return repo.findByDisplayName(username);
	}
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String displayName) throws UsernameNotFoundException {     
		User user = repo.findByDisplayName(displayName);
		if(user != null) {
        	return UserPrinciple.build(user);
        } else {
        	return null;
        }
	}

}
