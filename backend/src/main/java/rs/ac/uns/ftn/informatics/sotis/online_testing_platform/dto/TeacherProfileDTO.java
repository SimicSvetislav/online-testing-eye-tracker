package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import lombok.Data;

public @Data class TeacherProfileDTO {

	private Long id;
	private String fullName;
	private String displayName;
	private String email;
	private String phoneNumber;
	
	private Integer testsNum;
	
}
