package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import java.util.List;

import lombok.Data;

public @Data class TestingInfoDTO {
	
	private Long testId;
	private Long studentId;
	
	private List<TestAnswerDTO> testAnswers;
	
	// private List<DataRecordDTO> records;

}
