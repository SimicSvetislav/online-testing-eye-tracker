package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.FileUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.SotisApplication;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.QuestionDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.QuestionRegionSequenceDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.QuestionReportDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.RegionDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.ReportAnswerDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.ReportDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.TeacherProfileDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.TestDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Answer;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.AnswerElement;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.IdAnswerPair;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.ImageElement;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Question;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.QuestionNew;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.QuestionType;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Region;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Student;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Teacher;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Test;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.TestAnswer;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.TestAnswerEssay;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.TestAnswerMC;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.TestingInfo;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.RegionRepository;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.services.TeacherService;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.services.TestService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/teacher")
public class TeacherController {

	@Autowired
	private TeacherService service;
	
	@Autowired
    private ModelMapper modelMapper;
	
	@Autowired
	private TestService testService;
	
	@RequestMapping(method = RequestMethod.GET, path = "/profile/{id}", produces = "application/json")
	public ResponseEntity<TeacherProfileDTO> getTeacherProfile(@PathVariable("id") Long id) {
	
		Teacher teacher = service.findById(id); 
		
		TeacherProfileDTO dto = modelMapper.map(teacher, TeacherProfileDTO.class);
		
		dto.setTestsNum(teacher.getTests().size());
		
		return new ResponseEntity<TeacherProfileDTO>(dto, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/{id}/test/names", produces = "application/json")
	public ResponseEntity<List<TestDTO>> getTeacherTestsBasic(@PathVariable("id") Long id) {
		
		Teacher teacher = service.findById(id);
		
		List<Test> activeTests = teacher.getTests().stream().
				filter(x -> x.getActive()).
				collect(Collectors.toList());
		
		List<TestDTO> dtos = activeTests.stream().
				map(test -> modelMapper.map(test, TestDTO.class)).
				collect(Collectors.toList());
		
		return new ResponseEntity<List<TestDTO>>(dtos, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/test/details/{id}", produces = "application/json")
	public ResponseEntity<TestDTO> getTestDetails(@PathVariable("id") Long id) {
		
		Test test = testService.findById(id);
		
		TestDTO testDto = modelMapper.map(test, TestDTO.class);
		
		return new ResponseEntity<TestDTO>(testDto, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/test/details/new/{id}", produces = "application/json")
	public ResponseEntity<TestDTO> getTestDetailsNew(@PathVariable("id") Long id) {
		
		Test test = testService.findById(id);
		
		// System.out.println(test.toString());
		
		TestDTO testDto = modelMapper.map(test, TestDTO.class);
		
		for (QuestionNew qn: testDto.getQuestionElements()) {
			for (ImageElement ie: qn.getImageElements()) {
				String filename = ie.getImgURL();
				File imgFile = new File(SotisApplication.class.getClassLoader().getResource(".").getFile() + "/../../src/main/resources/static/images/" + filename);
				byte[] fileContent = null;
				try {
					fileContent = FileUtils.readFileToByteArray(imgFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
				String encodedString = Base64.getEncoder().encodeToString(fileContent);

				String imgUrl = "data:image/png;base64,"  + encodedString;
				
				ie.setImgURL(imgUrl);
			}
		}
		
		return new ResponseEntity<TestDTO>(testDto, HttpStatus.OK);
		
	}
	
	/*@RequestMapping(method = RequestMethod.GET, path = "/test/details/sorted/{id}", produces = "application/json")
	public ResponseEntity<TestDTO> getTestDetailsSortedRegions(@PathVariable("id") Long id) {
		
		Test test = testService.findById(id);
		
		TestDTO testDto = modelMapper.map(test, TestDTO.class);
		
		List<QuestionDTO> qdtos = testDto.getQuestions();
		List<QuestionDTO> qdtosSorted = new ArrayList<QuestionDTO>();
		
		for (QuestionDTO qdto: qdtos) {
			List<RegionDTO> regions = qdto.getRegions();
			regions.sort((r1, r2) -> r1.getId().compareTo(r2.getId()));
			qdto.setRegions(regions);
			qdtosSorted.add(qdto);
		}
		
		testDto.setQuestions(qdtosSorted);
		
		return new ResponseEntity<TestDTO>(testDto, HttpStatus.OK);
		
	}*/
	
	@RequestMapping(method = RequestMethod.GET, path = "/types", produces = "application/json")
	public ResponseEntity<List<QuestionType>> getQuestionTypes() {
		
		List<QuestionType> types = service.getQuestionTypes();
		
		return new ResponseEntity<List<QuestionType>>(types, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/test/{teacherId}", produces = "application/json")
	public ResponseEntity<String> postTest(@RequestBody Test test, @PathVariable("teacherId") Long teacherId) {
		
		Teacher teacher = service.findById(teacherId);
		test.setCreator(teacher);
		
		Test savedTest = testService.save(test);
		
		for (Question q: savedTest.getQuestions()) {
			
			for (Answer a: q.getAnswers()) {
				// a.setQuestion(q);
				a = testService.saveAnswer(a);
			}
			
			for (Region r: q.getRegions()) {
				//r.setQuestion(q);
				r = testService.saveRegion(r);
			}
		}
		
		Test t = testService.findById(savedTest.getId());
		
		List<Question> qs = t.getQuestions();
		for (Question q: qs) {
			
			for (Region r: q.getRegions()) {
				System.out.println(r.getHeight());
			}
			
			System.out.println(q.getRegions().size());
		}
		
		return new ResponseEntity<String>("Test successfully created", HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/test/new/{teacherId}", produces = "application/json")
	public ResponseEntity<String> postTestNew(@RequestBody Test test, @PathVariable("teacherId") Long teacherId) {
		
		Teacher teacher = service.findById(teacherId);
		test.setCreator(teacher);
		
		for (QuestionNew qn: test.getQuestionElements()) {
			for (ImageElement ie: qn.getImageElements()) {
				String imgURL = ie.getImgURL();
				String[] urlParts = imgURL.split(",");
				String base64Image = urlParts[1];
				String extension = urlParts[0].substring(urlParts[0].indexOf('/')+1, urlParts[0].indexOf(';'));
				System.out.println(extension);
				byte[] imageBytes = DatatypeConverter.parseBase64Binary(base64Image);
				
				String uuid = UUID.randomUUID().toString().replace("-", "");
				
				try {
					BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));
					File outputFile = new File(SotisApplication.class.getClassLoader().getResource(".").getFile() + "/../../src/main/resources/static/images/" + uuid + "." + extension);
					ImageIO.write(img, extension, outputFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				ie.setImgURL(uuid + "." + extension);
			}
		}
		
		
		
		Test savedTest = testService.save(test);
		
		/*for (Question q: savedTest.getQuestions()) {
			
			for (Answer a: q.getAnswers()) {
				a.setQuestion(q);
				a = testService.saveAnswer(a);
			}
			
			for (Region r: q.getRegions()) {
				// r.setQuestion(q);
				r = testService.saveRegion(r);
			}
		}*/
		
		/*for (QuestionNew q: savedTest.getQuestionElements()) {
			
			for (AnswerElement a: q.getAnswerElements()) {
				// a.setQuestion(q);
				a = testService.saveAnswerElement(a);
			}
			
			for (Region r: q.getRegions()) {
				// r.setQuestion(q);
				r = testService.saveRegion(r);
			}
		}*/
		
		Test t = testService.findById(savedTest.getId());
		
		/*List<QuestionNew> qs = t.getQuestionElements();
		for (QuestionNew q: qs) {
			
			for (Region r: q.getRegions()) {
				System.out.println(r.getHeight());
			}
			
			System.out.println(q.getRegions().size());
		}*/
		
		return new ResponseEntity<String>("Test successfully created", HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/results/{id}", produces = "application/json")
	public ResponseEntity<List<ReportDTO>> resultsForTeacher(@PathVariable("id") Long id) {
		
		List<ReportDTO> dtos = new ArrayList<ReportDTO>();
		
 		List<TestingInfo> tis = testService.findTestingInfoByTeacher(id);
		
		Map<String, Integer> attemptsMap = new HashMap<String, Integer>();
		
		// Sort
		tis.sort((ti1, ti2) -> ti1.getId().compareTo(ti2.getId()));
		
		for (TestingInfo ti: tis) {
			
			ReportDTO dto = modelMapper.map(ti, ReportDTO.class);
			
			Test test = ti.getTest();
			Student student = ti.getStudent();
			
			dto.setTestId(test.getId());
			dto.setTestName(test.getName());
			dto.setStudentIndex(student.getIndexNum());
			dto.setQuestionsNum(ti.getTest().getQuestionElements().size());
			
			int attempt = 1;
			String mapKey = student.getIndexNum() + "_" + test.getId();
			if (attemptsMap.containsKey(mapKey)) {
				attempt = attemptsMap.get(mapKey) + 1;
				attemptsMap.put(mapKey, attempt); 
			} else {
				attemptsMap.put(mapKey, 1);
			}
			
			dto.setAttempt(attempt);
			
			dtos.add(dto);
		}
		
		return new ResponseEntity<List<ReportDTO>>(dtos, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/results/answers/{id}", produces = "application/json")
	public ResponseEntity<ReportAnswerDTO> getResultAnswers(@PathVariable("id") Long id) {
		
		ReportAnswerDTO dto = new ReportAnswerDTO();
		dto.setId(id);
		
		TestingInfo ti = testService.findTestingInfoById(id);
		List<QuestionReportDTO> qrs = new ArrayList<QuestionReportDTO>();
		
		List<TestAnswer> tas = ti.getTestAnswers();
		
		for (TestAnswer ta: tas) {
			QuestionReportDTO qr = new QuestionReportDTO();
			qr.setCorrect(ta.getCorrect());
			qr.setQTypeId(ta.getQuestion().getType().getId());
			// qr.setText(ta.getQuestion().getText());
			qr.setText(ta.getQuestion().getTitleElements().get(0).getTitleText());
			
			// List<Answer> answers = ta.getQuestion().getAnswers();
			List<AnswerElement> answers = ta.getQuestion().getAnswerElements();
			
			List<String> correctAnswers = answers.stream().
					filter(a -> a.getCorrect()).
					map(a -> a.getText()).
					collect(Collectors.toList());
			
			qr.setCorrectAnswer(correctAnswers);
			
			List<String> givenAnswers = new ArrayList<String>();
			
			if (ta instanceof TestAnswerMC) {
				// System.out.println("MC");
				TestAnswerMC tamc = (TestAnswerMC) ta;
				for (IdAnswerPair idap: tamc.getIdAnswerPairs()) {
					if (idap.getGivenAnswer()) {
						AnswerElement answer = answers.stream()
						  .filter(a -> idap.getAnswerId().equals(a.getId()))
						  .findAny()
						  .orElse(null);
						
						givenAnswers.add(answer.getText());
						
					}
				}
				
				qr.setAnswered(givenAnswers);
				
			} else if (ta instanceof TestAnswerEssay) {
				// System.out.println("E");
				TestAnswerEssay tae = (TestAnswerEssay) ta;
				givenAnswers.add(tae.getEssayAnswer());
				qr.setAnswered(givenAnswers);
				qr.setCorrectAnswer(new ArrayList<String>());
			}
			
			qrs.add(qr);
		}
		
		dto.setQReports(qrs);
		
		List<QuestionRegionSequenceDTO> rs = testService.getRegionSequence_v2(ti.getId());
		
		dto.setQrs(rs);
		
		return new ResponseEntity<ReportAnswerDTO>(dto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/csv/{id}")
	public void fooAsCSV(@PathVariable("id") Long id, HttpServletResponse response) {         
	    response.setContentType("text/plain; charset=utf-8");
	    try {
			response.getWriter().print("a,b,c\n1,2,3\n3,4,5");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Autowired
	private RegionRepository rr;
	
	@RequestMapping(method = RequestMethod.GET, path = "/norm", produces = "application/json")
	public ResponseEntity<List<Region>> normalizeRegions() {
	    
		List<Region> norms = normalizeRectangles(rr.findAll());
		
    	rr.saveAll(norms);
		
        return new ResponseEntity<List<Region>>(norms, HttpStatus.OK);
	}
	
	/*@RequestMapping(method = RequestMethod.GET, path = "/debug1/{id}", produces = "application/json")
	public ResponseEntity<String> debug1(@PathVariable("id") Long id) {

		Test t = testService.findById(id);
		
		List<Question> qs = t.getQuestions();
		for (Question q: qs) {
			
			for (Region r: q.getRegions()) {
				System.out.println(r.getHeight());
			}
			System.out.println(q.getRegions().size());
			
			for (Answer a: q.getAnswers()) {
				System.out.println(a.getText());
			}
			System.out.println(q.getAnswers().size());
			
		}
		
		return new ResponseEntity<String>("OK", HttpStatus.OK);
		
	}*/
	
	@GetMapping(value = "/csv2/{tiId}", produces = "text/csv")
	public ResponseEntity<FileSystemResource> generateReport(@PathVariable(value = "tiId") Long tiId) {
	    
    	TestingInfo testingInfo = testService.getTestingInfoById(tiId);
		String filename = testingInfo.getEyeTrackerDataFile();
		File csvFile = new File(SotisApplication.class.getClassLoader().getResource(".").getFile() + "/../../src/main/resources/" + filename + ".csv");
		
        return ResponseEntity.ok()
                .header("Content-Disposition", "attachment; filename=" + filename + ".csv")
                .contentLength(csvFile.length())
                .contentType(MediaType.parseMediaType("text/csv"))
                .body(new FileSystemResource(csvFile));
	}
	
	private List<Region>  normalizeRectangles(List<Region> regions) {
		List<Region>  retVal = new ArrayList<Region>();

		for (Region rect: regions) {
			
	      if (rect.getHeight() > 0 && rect.getWidth() > 0) {
	        retVal.add(rect);
	      } else {
	        Region newRect = new Region();
	        newRect.setId(rect.getId());

	        if (rect.getHeight() < 0) {
	          newRect.setHeight(Math.abs(rect.getHeight()));
	          newRect.setY(rect.getY() + rect.getHeight());
	        } else {
	          newRect.setY(rect.getY());
	          newRect.setHeight(rect.getHeight());
	        }

	        if (rect.getWidth() < 0) {
	          newRect.setWidth(Math.abs(rect.getWidth()));
	          newRect.setX(rect.getX() + rect.getWidth());
	        } else {
	          newRect.setX(rect.getX());
	          newRect.setWidth(rect.getWidth());
	        }

	        retVal.add(newRect);
	      }
	    }

	    return retVal;
	  }
	
}
