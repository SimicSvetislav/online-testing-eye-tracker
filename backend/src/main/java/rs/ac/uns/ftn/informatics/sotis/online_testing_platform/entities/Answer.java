package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity
public @Data class Answer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String text;
	private Boolean correct;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Question question;
	
}
