package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import java.util.List;

import lombok.Data;

public @Data class ReportAnswerDTO {

	private Long id;
	
	private List<QuestionReportDTO> qReports;
	private List<QuestionRegionSequenceDTO> qrs;
	
}
