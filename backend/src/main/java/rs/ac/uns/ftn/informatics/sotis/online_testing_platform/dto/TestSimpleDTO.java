package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import lombok.Data;

public @Data class TestSimpleDTO {

	private Long id;
	private String name;
	private Integer duration;
	
	private Integer questionsNum;
	
}
