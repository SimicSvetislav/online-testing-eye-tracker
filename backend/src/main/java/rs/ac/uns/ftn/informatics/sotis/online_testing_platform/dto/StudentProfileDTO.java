package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import lombok.Data;

public @Data class StudentProfileDTO {

	private Long id;
	
	private String fullName;
	private String displayName;
	private String email;
	
	private String indexNum;
	
	private Integer solvedTests;
	private Integer solvedTestsUnique;
	
}
