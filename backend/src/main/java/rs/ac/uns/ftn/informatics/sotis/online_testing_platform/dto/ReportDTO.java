package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import lombok.Data;

public @Data class ReportDTO {

	private Long id;
	private Long testId;
	private String testName;
	private Integer attempt;
	private Integer correctAnswers;
	private Integer essayQuestions;
	private Integer questionsNum;
	private String studentIndex;
	
	// private List<TestAnswer> testAnswers;

}
