package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import lombok.Data;

public @Data class JwtResponse {
    private String token;
    private String type = "Bearer";
    private String displayName;
	private Collection<? extends GrantedAuthority> authorities;
	//private Integer idCompany;
	private Long id;

	public JwtResponse(String accessToken, String displayName, Collection<? extends GrantedAuthority> authorities, Long id) {
		this.token = accessToken;
		this.displayName = displayName;
		this.authorities = authorities;
		//this.idCompany = id;
		this.id = id;
	}
}
