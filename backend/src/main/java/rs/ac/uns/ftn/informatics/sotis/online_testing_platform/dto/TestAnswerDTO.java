package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import lombok.Data;

public @Data class TestAnswerDTO {

	protected Long questionId;
	
	protected Object answer;
	
}
