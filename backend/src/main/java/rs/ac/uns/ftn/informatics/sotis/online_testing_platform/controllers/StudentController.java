package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.SotisApplication;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.AnswerDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.DataRecordDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.QuestionDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.ReportDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.SimpleReportDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.StudentProfileDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.TestAnswerDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.TestDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.TestSimpleDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.TestingInfoDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Answer;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.AnswerElement;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.IdAnswerPair;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.ImageElement;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Question;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.QuestionNew;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Student;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Test;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.TestAnswer;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.TestAnswerEssay;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.TestAnswerMC;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.TestingInfo;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.services.StudentService;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.services.TestService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/student")
public class StudentController {
	
	private final static Logger logger = LoggerFactory.getLogger(StudentController.class);
	
	@Autowired
	private StudentService service;
	
	@Autowired
	private TestService testService;
	
	@Autowired
	private ModelMapper mapper;
	
	@RequestMapping(method = RequestMethod.GET, path = "/profile/{id}", produces = "application/json")
	public ResponseEntity<StudentProfileDTO> getStudentProfile(@PathVariable("id") Long id) {
	
		Student student = service.findById(id); 
		
		StudentProfileDTO dto = mapper.map(student, StudentProfileDTO.class);
		
		List<TestingInfo> results = testService.getTestingInfoByStudentId(id);
		
		dto.setSolvedTests(results.size());
		
		Set<Test> tests = results.
				stream().
				map(r -> r.getTest()).
				collect(Collectors.toSet());
		
		dto.setSolvedTestsUnique(tests.size());
		
		return new ResponseEntity<StudentProfileDTO>(dto, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/tests/names", produces = "application/json")
	public ResponseEntity<List<TestSimpleDTO>> getTestNames() {
	
		List<Test> tests = testService.findAll();
		
		List<Test> activeTests = tests.stream().
				filter(x -> x.getActive()).
				collect(Collectors.toList());
		
		List<TestSimpleDTO> dtos = activeTests.stream().
				map(test -> mapper.map(test, TestSimpleDTO.class)).
				collect(Collectors.toList());
		
		for (TestSimpleDTO dto: dtos) {
			for (Test test: activeTests) {
				if(dto.getId().equals(test.getId())) {
					// dto.setQuestionsNum(test.getQuestions().size());
					dto.setQuestionsNum(test.getQuestionElements().size());
					break;
				}
			}
		}
		
		return new ResponseEntity<List<TestSimpleDTO>>(dtos, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/test/details/{id}", produces = "application/json")
	public ResponseEntity<TestDTO> getTestDetails(@PathVariable("id") Long id) {
		
		Test test = testService.findById(id);
		
		TestDTO testDto = mapper.map(test, TestDTO.class);
		
		for (QuestionNew q: testDto.getQuestionElements()) {
			for (AnswerElement a: q.getAnswerElements()) {
				a.setCorrect(null);
			}
		}
		
		for (QuestionNew qn: testDto.getQuestionElements()) {
			for (ImageElement ie: qn.getImageElements()) {
				String filename = ie.getImgURL();
				File imgFile = new File(SotisApplication.class.getClassLoader().getResource(".").getFile() + "/../../src/main/resources/static/images/" + filename);
				byte[] fileContent = null;
				try {
					fileContent = FileUtils.readFileToByteArray(imgFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
				String encodedString = Base64.getEncoder().encodeToString(fileContent);

				String imgUrl = "data:image/png;base64,"  + encodedString;
				
				ie.setImgURL(imgUrl);
			}
		}
		
		return new ResponseEntity<TestDTO>(testDto, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/test/submit", produces = "application/json")
	public ResponseEntity<SimpleReportDTO> submitTest(@RequestBody TestingInfoDTO testingInfo) {
		
		Test test = testService.findById(testingInfo.getTestId());
		Student student = service.findById(testingInfo.getStudentId());
		
		int correctAnswers = 0;
		int essayQuestions = 0;
		
		List<TestAnswer> testAnswersDB = new ArrayList<TestAnswer>();
		
		for (TestAnswerDTO ta: testingInfo.getTestAnswers()) {
			
			QuestionNew q = testService.findNewQuestionById(ta.getQuestionId());
			// TestAnswer testAnswerDB = new TestAnswer();
			
			if (q.getType().getId().equals(1L)) {
				@SuppressWarnings("unchecked")
				List<LinkedHashMap<Object, Object>> givenAnswers = (ArrayList<LinkedHashMap<Object, Object>>) ta.getAnswer();
				
				TestAnswerMC testAnswerDB = new TestAnswerMC();
				testAnswerDB.setQuestion(q);
				
				List<IdAnswerPair> idAnswerPairs = new ArrayList<IdAnswerPair>();
				
				boolean correctAnswer = true;
				
				for(LinkedHashMap<Object, Object> ga: givenAnswers) {
					Boolean gaBool = (Boolean) ga.get("givenAnswer");
					Long gaAnswerId = new Long(ga.get("answerId").toString());
					
					// System.out.println(gaBool);
					// System.out.println(gaAnswerId);
					
					IdAnswerPair idAP = new IdAnswerPair();
					idAP.setGivenAnswer(gaBool);
					idAP.setAnswerId(gaAnswerId);
					idAnswerPairs.add(idAP);
					
					AnswerElement answerDB = testService.findAnswerElementById(gaAnswerId);
					
					if (!answerDB.getCorrect().equals(gaBool)) {
						correctAnswer = false;
						break;
					}
				}
				
				if (correctAnswer) {
					correctAnswers++;
				}
				
				testAnswerDB.setCorrect(correctAnswer);
				testAnswerDB.setIdAnswerPairs(idAnswerPairs);
				
				testAnswersDB.add(testAnswerDB);
				
			} else if (q.getType().getId().equals(2L)) {
				String answer = (String) ta.getAnswer();
				//testAnswerDB.set
				TestAnswerEssay testAnswerDB = new TestAnswerEssay();
				testAnswerDB.setQuestion(q);
				testAnswerDB.setEssayAnswer(answer);
				essayQuestions++;
				// System.out.println(answer);
				testAnswersDB.add(testAnswerDB);
			} else {
				throw new ResponseStatusException(HttpStatus.I_AM_A_TEAPOT, "Answer type not supported");
			}
		}
		
		String filename = student.getIndexNum() + "_" + new Date().getTime();
		
		TestingInfo testingInfoDB = new TestingInfo();
		testingInfoDB.setTest(test);
		testingInfoDB.setStudent(student);
		testingInfoDB.setTestAnswers(testAnswersDB);
		testingInfoDB.setCorrectAnswers(correctAnswers);
		testingInfoDB.setEssayQuestions(essayQuestions);
		testingInfoDB.setEyeTrackerDataFile(filename);
		
		testingInfoDB = testService.saveTestingInfo(testingInfoDB);
		logger.info("Testing data saved");
		
		SimpleReportDTO report = mapper.map(testingInfoDB, SimpleReportDTO.class);
		report.setTestName(test.getName());
		report.setQuestions(test.getQuestionElements().size());
		
		return new ResponseEntity<SimpleReportDTO>(report, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/test/records/{tiId}", produces = "application/json")
	public ResponseEntity<String> submitRecords(@RequestBody List<DataRecordDTO> records,
													  @PathVariable("tiId") Long tiId) {
		
		TestingInfo testingInfo = testService.getTestingInfoById(tiId);
		String filename = testingInfo.getEyeTrackerDataFile();
		File csvFile = new File(SotisApplication.class.getClassLoader().getResource(".").getFile() + "/../../src/main/resources/" + filename + ".csv");
		try {
			csvFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Writing to file " + filename);
		String csvLine =  	"cnt," +
							"time," +
							"timetick," +
						    
							"fpogx," +
						    "fpogy," +
						    "fpogs," + 
						    "fpogd," +
						    "fpogId," +
						    "fpogv," +
						    
						    "bpogx," +
						    "bpogy," +
						    "bpogv," +
						    
						    "lpcx," +
						    "lpcy," +
						    "lpd," +
						    "lps," +
						    "lpv," +
						    
						    "rpcx," +
						    "rpcy," +
						    "rpd," +
						    "rps," +
						    "rpv," +
						    
							"cx," +
						    "cy," +
						    "cs," +
						    
						    "questionId\n";
		int i = 0;
		
		try (PrintWriter writer = new PrintWriter(csvFile)) {
			writer.write(csvLine);
			for (DataRecordDTO dr: records) {
				csvLine = dr.toString() + "\n";
				writer.write(csvLine);
				if (++i % 10000 == 0) {
					System.out.println(i);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		String msg = "Data records saved";
		logger.info(msg);
		
		return new ResponseEntity<String>(msg, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/results/{id}", produces = "application/json")
	public ResponseEntity<List<ReportDTO>> resultsForStudent(@PathVariable("id") Long id) {
		
		List<ReportDTO> dtos = new ArrayList<ReportDTO>();
		
		List<TestingInfo> tis = testService.findTestingInfoByStudent(id);
		
		Map<Long, Integer> attemptsMap = new HashMap<Long, Integer>();
		
		// Sort
		tis.sort((ti1, ti2) -> ti1.getId().compareTo(ti2.getId()));
		
		for (TestingInfo ti: tis) {
			
			ReportDTO dto = mapper.map(ti, ReportDTO.class);
			
			Test test = ti.getTest();
			
			dto.setTestName(test.getName());
			dto.setQuestionsNum(ti.getTest().getQuestionElements().size());
			
			Long testId = test.getId();
			
			int attempt = 1;
			if (attemptsMap.containsKey(testId)) {
				attempt = attemptsMap.get(testId) + 1;
				attemptsMap.put(testId, attempt);
			} else {
				attemptsMap.put(testId, 1);
			}
			
			dto.setAttempt(attempt);
			
			dtos.add(dto);
		}
		
		return new ResponseEntity<List<ReportDTO>>(dtos, HttpStatus.OK);
		
	}

}
