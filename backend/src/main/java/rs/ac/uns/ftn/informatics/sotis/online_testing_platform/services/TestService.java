package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.SotisApplication;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.QuestionRegionSequenceDTO;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Answer;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.AnswerElement;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.DataRecordSimple;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.QuestionNew;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Region;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.Test;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.TestingInfo;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.AnswerElementRepository;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.AnswerRepository;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.QuestionNewRepository;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.QuestionRepository;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.RegionRepository;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.TestRepository;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories.TestingInfoRepository;

@Service
public class TestService {

	@Autowired
	private TestRepository repo;
	
	@Autowired
	private QuestionRepository qRepo;
	
	@Autowired
	private RegionRepository regionRepo;
	
	@Autowired
	private AnswerRepository answerRepo;
	
	@Autowired
	private TestingInfoRepository tiRepo;
	
	@Autowired
	private QuestionNewRepository qnRepo;
	
	@Autowired
	private AnswerElementRepository aeRepo;

	/*@Autowired
	private AnswerElementRepository answerElRepo;*/
	
	public Test findById(Long id) {
		Test test = repo.findById(id).get();
		return test;
	}
	
	public Test save(Test test) {
		return repo.save(test);
	}
	
	/*public Question saveQuestion(Question q) {
		return qRepo.save(q);
	}*/
	
	public Region saveRegion(Region r) {
		return regionRepo.save(r);
	}
	
	public Answer saveAnswer(Answer a) {
		return answerRepo.save(a);
	}

	public List<Test> findAll() {
		return repo.findAll();
	}

	/*public Question findQuestionById(Long questionId) {
		return qRepo.getOne(questionId);
	}*/

	public Answer findAnswerById(Long answerId) {
		return answerRepo.getOne(answerId);
	}

	public TestingInfo saveTestingInfo(TestingInfo ti) {
		return tiRepo.save(ti);
		
	}

	public List<TestingInfo> getTestingInfoByStudentId(Long id) {
		return tiRepo.findByStudentId(id);
	}

	public List<TestingInfo> findTestingInfoByStudent(Long id) {
		return tiRepo.findByStudentId(id);
	}

	public List<TestingInfo> findTestingInfoByTeacher(Long id) {
		
		List<TestingInfo> tisAll = tiRepo.findAll();
		List<TestingInfo> tis = new ArrayList<TestingInfo>();
		
		for (TestingInfo ti: tisAll) {
			if (ti.getTest() != null) {
				if (ti.getTest().getCreator().getId().equals(id)) {
					tis.add(ti);
				}
			}
		}
		return tis;
	}

	public TestingInfo findTestingInfoById(Long id) {
		return tiRepo.getOne(id);
	}
	
	/*public RegionSequenceDTO getRegionSequence(Long tiId) {
		RegionSequenceDTO rs = new RegionSequenceDTO();
		
		TestingInfo ti = tiRepo.getOne(tiId);
		
		String filename = ti.getEyeTrackerDataFile();
		
		List<DataRecordSimple> records = readCSV(filename);
		
		Map<Long, Question> questionMap = new HashMap<Long, Question>();
		Map<Long, Integer> visitMap = new HashMap<Long, Integer>();
		Integer fixationId = null;
		DataRecordSimple begin = null;
		DataRecordSimple end = null;
		
		List<QuestionRegionSequenceDTO> listSequences = new ArrayList<QuestionRegionSequenceDTO>();
		QuestionRegionSequenceDTO activeQRS = new QuestionRegionSequenceDTO();
		List<Long> activeRegionSequence = new ArrayList<Long>();
		Boolean newQuestion = true;
		
		// TODO: Uzeti u obzir razlicita pitanja
		// TODO: Proveriti da li je poslednji element u sekvenci isti kao i novi
		// (ako sekvenca nije prazna)
		// TODO: Proveriti poslednji element kao specijalni slucaj 
		for (DataRecordSimple dr: records) {
			
			// Pronadji pitanje
			Long qId = dr.getQuestionId();
			Question question = null;
			if ((question = questionMap.get(qId)) == null) {
				question = qRepo.getOne(qId);
				List<Region> regionsLocal = question.getRegions();
				// regionsLocal.sort((r1, r2) -> r1.getId().compareTo(r2.getId()));
				question.setRegions(regionsLocal);
				questionMap.put(qId, question);
			}
			
			// Proveriti da li je pitanje promenjeno
			if (!activeQRS.getQuestionId().equals(qId)) {
				// Ako je novo pitanje proveriti postojecu fiksaciju
				// Dodati je ako treba
				// podesiti begin na trenutni zapis
				// Odmah proveri pripadnost regionu
				// Inkrementiraj zapis u visitMap
				if (visitMap.containsKey(activeQRS.getQuestionId())) {
					visitMap.put(activeQRS.getQuestionId(), 
							visitMap.get(activeQRS.getQuestionId()) + 1);
				} else {
					visitMap.put(activeQRS.getQuestionId(), 1);
				}
				
				// Dodaj QRS
				
				activeQRS = new QuestionRegionSequenceDTO();
				activeQRS.setQuestionId(qId);
				
			} else {
				// Ako pitanje nije novo
				// Pripadnost regionu se proverava kada stignemo do kraja fiksacije
				Integer fpogv = dr.getFpogv();
				if (fpogv.equals(0)) {
					if (fixationId == null) {
						// Ako nije validna fiksacija i nema aktivne nastavi dalje
						continue;
					} else {
						// Ako nije validna fiksacija i postoji aktivna proveriti regione
						// Za prvi i poslednji zapis
						List<Region> regions = question.getRegions();
						
						List<Long> regionsForSequence = new ArrayList<Long>();
						List<Long> hitRegions = checkRegions(regions, dr);
						
						// Zabelezi da nema aktivne fiksacije
						fixationId = null;
						
					}
				} else {
					if (fixationId == null) {
						// Ako je fiksacija validna i nema aktivne zapocni novu
						begin = dr;
						fixationId = dr.getFpogId();
					} else {
						// Ako je fiksacija validna i postoji aktivne zabelezi aktuelnu kao poslednju
						end = dr;
					}
				}
			}
			
					
		}
		
		System.out.println(filename);
		
		return rs;
	}*/
	
	public List<QuestionRegionSequenceDTO> getRegionSequence_v2(Long tiId) {
		// RegionSequenceDTO rs = new RegionSequenceDTO();
		
		TestingInfo ti = tiRepo.getOne(tiId);
		
		String filename = ti.getEyeTrackerDataFile();
		
		List<DataRecordSimple> records = readCSV(filename);
		
		Map<Long, QuestionNew> questionMap = new HashMap<Long, QuestionNew>();
		// Map<Long, Integer> visitMap = new HashMap<Long, Integer>();
		DataRecordSimple startRecord = null;
		DataRecordSimple endRecord = null;

		Integer i = 0;
		
		List<QuestionRegionSequenceDTO> finalSequence = new ArrayList<QuestionRegionSequenceDTO>();
		List<List<Long>> regionSequence = new ArrayList<List<Long>>();
		
		// Pronadji prvi validni pocetak
		for (DataRecordSimple drs: records) {
			++i;
			if (drs.getFpogv().equals(1)) {
				startRecord = drs;
				break;
			}
		}
		
		for (; i<records.size(); ++i) {
			// Uslovi za proveru da li sekvenca pripada nekom regionu:
			// 1. Promenjeno pitanje
			// 2. Nevalidna fiksacija
			// 3. Novi ID fiksacije
			
			DataRecordSimple record = records.get(i);
			
			if (!startRecord.getQuestionId().equals(record.getQuestionId())) {
				// Novo pitanje
				// Poslednji je prethodni
				endRecord = records.get(i-1);
				
				QuestionNew questionNew = null;
				// Integer visitNum;
				
				if ((questionNew = questionMap.get(startRecord.getQuestionId())) == null) {
					questionNew = qnRepo.getOne(startRecord.getQuestionId());
					// List<Region> regionsLocal = question.getRegions();
					// regionsLocal.sort((r1, r2) -> r1.getId().compareTo(r2.getId()));
					// question.setRegions(regionsLocal);
					questionMap.put(questionNew.getId(), questionNew);
					
					//visitNum = 1;
					//visitMap.put(question.getId(), visitNum);
					
				} else {
					//visitNum = visitMap.get(question.getId()) + 1;
					//visitMap.put(question.getId(), visitNum);
				}
				
				List<Long> startRegions = checkRegions(questionNew.getRegions(), startRecord);
				List<Long> endRegions = checkRegions(questionNew.getRegions(), endRecord);
				
				if (startRegions.equals(endRegions)) {
					// Fiksacija u prvom i poslednjem trenutku je u istim regionima
					// Proveriti da li su to isti kao i poslednje zabelezeni regioni
					// Ako nisu treba ih dodati, ako jesu ne treba
					if (regionSequence.size() > 0) {
						List<Long> lastRegions = regionSequence.get(regionSequence.size() - 1);
						if (!lastRegions.equals(startRegions)) {
							regionSequence.add(startRegions);
						}
					}  else {
						regionSequence.add(startRegions);
					}
				}
				
				// Dodavanje u finalnu sekvencu
				if (regionSequence.size() > 0 ) {
					QuestionRegionSequenceDTO qrs = new QuestionRegionSequenceDTO();
					qrs.setQuestionId(questionNew.getId());
					qrs.setRegionSequence(regionSequence);
					finalSequence.add(qrs);
				}
				
				regionSequence = new ArrayList<List<Long>>(); 
				
				// Novi pocetni je trenutni, ako je validan
				/*if (record.getFpogv().equals(1)) {
					startRecord = record;
				} else {
					// Pronadji prvi sledeci validan
					while 
				}*/
				
				while (true) {
					record = records.get(i);
					if (record.getFpogv().equals(0) && !i.equals(records.size()-1)) {
						++i;
					} else {
						break;
					}
					
				}
				
				startRecord = record;
				
			} else if (record.getFpogv().equals(0)) {
				// Nevalidna fiksacija
				// Poslednji je prethodni
				endRecord = records.get(i-1);
				
				QuestionNew questionNew = null;
				// Integer visitNum;
				
				if ((questionNew = questionMap.get(startRecord.getQuestionId())) == null) {
					questionNew = qnRepo.getOne(startRecord.getQuestionId());
					// List<Region> regionsLocal = question.getRegions();
					// regionsLocal.sort((r1, r2) -> r1.getId().compareTo(r2.getId()));
					// question.setRegions(regionsLocal);
					questionMap.put(questionNew.getId(), questionNew);
					
					// visitNum = 1;
					// visitMap.put(question.getId(), visitNum);
					
				}
				// Ne treba dodavati broj poseta
				// Dodaje se kada se pitanje promeni
				/* else {
					visitNum = visitMap.get(question.getId()) + 1;
					visitMap.put(question.getId(), visitNum);
				}*/
				
				List<Long> startRegions = checkRegions(questionNew.getRegions(), startRecord);
				List<Long> endRegions = checkRegions(questionNew.getRegions(), endRecord);
				
				if (!startRegions.isEmpty() && startRegions.equals(endRegions)) {
					// Fiksacija u prvom i poslednjem trenutku je u istim regionima
					// Proveriti da li su to isti kao i poslednje zabelezeni regioni
					// Ako nisu treba ih dodati, ako jesu ne treba
					if (regionSequence.size() > 0) {
						List<Long> lastRegions = regionSequence.get(regionSequence.size() - 1);
						if (!lastRegions.equals(startRegions)) {
							regionSequence.add(startRegions);
						}
					}  else {
						regionSequence.add(startRegions);
					}
				}
				
				if (i.equals(records.size() - 1)) {
					break;
				}
				
				// Na indeksu i je nevalidna fiksacija
				++i;
				
				while (true) {
					record = records.get(i);
					if (record.getFpogv().equals(0) && !i.equals(records.size()-1)) {
						++i;
					} else {
						if (regionSequence.size() > 0 ) {
							if (!record.getQuestionId().equals(questionNew.getId())) {
								QuestionRegionSequenceDTO qrs = new QuestionRegionSequenceDTO();
								qrs.setQuestionId(questionNew.getId());
								qrs.setRegionSequence(regionSequence);							
								finalSequence.add(qrs);
							} else if (i.equals(records.size() - 1)) {
								QuestionRegionSequenceDTO qrs = new QuestionRegionSequenceDTO();
								qrs.setQuestionId(questionNew.getId());
								// qrs.setVisitNum(visitMap.get(question.getId()));
								qrs.setRegionSequence(regionSequence);
								// Nije neophodno
								// regionSequence = new ArrayList<List<Long>>();
								finalSequence.add(qrs);
							}
						}
						regionSequence = new ArrayList<List<Long>>();
						break;
					}
					
				}

				startRecord = record;
				
			} else if (!startRecord.getFpogId().equals(record.getFpogId())) {
				// Nova fiksacija
				// Poslednji je prethodni
				endRecord = records.get(i-1);
				
				QuestionNew questionNew = null;
				// Integer visitNum;
				
				if ((questionNew = questionMap.get(startRecord.getQuestionId())) == null) {
					questionNew = qnRepo.getOne(startRecord.getQuestionId());
					// List<Region> regionsLocal = question.getRegions();
					// regionsLocal.sort((r1, r2) -> r1.getId().compareTo(r2.getId()));
					// question.setRegions(regionsLocal);
					questionMap.put(questionNew.getId(), questionNew);
					
					// visitNum = 1;
					// visitMap.put(question.getId(), visitNum);
					
				} else {
					// visitNum = visitMap.get(question.getId()) + 1;
					// visitMap.put(question.getId(), visitNum);
				}
				
				List<Long> startRegions = checkRegions(questionNew.getRegions(), startRecord);
				List<Long> endRegions = checkRegions(questionNew.getRegions(), endRecord);
				
				if (startRegions.equals(endRegions)) {
					// Fiksacija u prvom i poslednjem trenutku je u istim regionima
					// Proveriti da li su to isti kao i poslednje zabelezeni regioni
					// Ako nisu treba ih dodati, ako jesu ne treba
					if (regionSequence.size() > 0) {
						List<Long> lastRegions = regionSequence.get(regionSequence.size() - 1);
						if (!lastRegions.equals(startRegions)) {
							regionSequence.add(startRegions);
						}
					}  else {
						regionSequence.add(startRegions);
					}
				}
				
				while (record.getFpogv().equals(0) && !i.equals(records.size()-1)) {
					record = records.get(i++);
				}
				
				if (!record.getQuestionId().equals(questionNew.getId())) {
					if (regionSequence.size() > 0 ) {
						QuestionRegionSequenceDTO qrs = new QuestionRegionSequenceDTO();
						qrs.setQuestionId(questionNew.getId());
						qrs.setRegionSequence(regionSequence);
						finalSequence.add(qrs);
					}
					regionSequence = new ArrayList<List<Long>>();
				}
				
				if (i.equals(records.size() - 1)) {
					if (regionSequence.size() > 0 ) {
						QuestionRegionSequenceDTO qrs = new QuestionRegionSequenceDTO();
						qrs.setQuestionId(questionNew.getId());
						// qrs.setVisitNum(visitMap.get(question.getId()));
						qrs.setRegionSequence(regionSequence);
						finalSequence.add(qrs);
					}
					regionSequence = new ArrayList<List<Long>>();
				}
				
				startRecord = record;
				
			} else if (i.equals(records.size()-1)) {
				// Poslednji zapis
				endRecord = record;
				
				QuestionNew questionNew = null;
				// Integer visitNum;
				
				if ((questionNew = questionMap.get(startRecord.getQuestionId())) == null) {
					questionNew = qnRepo.getOne(startRecord.getQuestionId());
					// List<Region> regionsLocal = question.getRegions();
					// regionsLocal.sort((r1, r2) -> r1.getId().compareTo(r2.getId()));
					// question.setRegions(regionsLocal);
					questionMap.put(questionNew.getId(), questionNew);
					
					// visitNum = 1;
					// visitMap.put(question.getId(), visitNum);
					
				} 
				/*else {
					visitNum = visitMap.get(question.getId()) + 1;
					visitMap.put(question.getId(), visitNum);
				}*/
				
				List<Long> startRegions = checkRegions(questionNew.getRegions(), startRecord);
				List<Long> endRegions = checkRegions(questionNew.getRegions(), endRecord);
				
				if (startRegions.equals(endRegions)) {
					// Fiksacija u prvom i poslednjem trenutku je u istim regionima
					// Proveriti da li su to isti kao i poslednje zabelezeni regioni
					// Ako nisu treba ih dodati, ako jesu ne treba
					if (regionSequence.size() > 0) {
						List<Long> lastRegions = regionSequence.get(regionSequence.size() - 1);
						if (!lastRegions.equals(startRegions)) {
							regionSequence.add(startRegions);
						}
					}  else {
						regionSequence.add(startRegions);
					}
				}
				
				// Dodavanje u finalnu sekvencu
				if (regionSequence.size() > 0 ) {
					QuestionRegionSequenceDTO qrs = new QuestionRegionSequenceDTO();
					qrs.setQuestionId(questionNew.getId());
					qrs.setRegionSequence(regionSequence);
					finalSequence.add(qrs);
				}
			}
			
			
		}
	
		return finalSequence;
	}

	private List<Long> checkRegions(List<Region> regions, DataRecordSimple record) {
		
		List<Long> hitRegions = new ArrayList<Long>();
		
		for (Region region: regions) {
			if (record.getFpogx() > region.getX() && 
					record.getFpogx() < region.getX() + region.getWidth() && 
					record.getFpogy() > region.getY() &&
					record.getFpogy() < region.getY() + region.getHeight()) {
				
				hitRegions.add(region.getId());
				
			}
		}
		
		return hitRegions;
		
	}
	
	private List<DataRecordSimple> readCSV(String filename) {
		
		List<DataRecordSimple> records = new ArrayList<DataRecordSimple>();
		
		File csvFile = new File(SotisApplication.class.getClassLoader().getResource(".").getFile() + "/../../src/main/resources/" + filename + ".csv");
		
        String line = "";
        
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
        	
        	// Header
        	line = br.readLine();
        	
        	int i = 0;
        	// int c = 2;
            while ((line = br.readLine()) != null) {

                String[] record = line.split(",");
                
                Long cnt = Long.parseLong(record[0].split("\\.")[0]);
                Double fpogx = Double.parseDouble(record[3]);
                Double fpogy = Double.parseDouble(record[4]);
                Double fpogs = Double.parseDouble(record[5]);
                Double fpogd = Double.parseDouble(record[6]);
                Integer fpogId = Integer.parseInt(record[7].split("\\.")[0]);
                Integer fpogv = Integer.parseInt(record[8].split("\\.")[0]);
                Long questionId = Long.parseLong(record[25].split("\\.")[0]);
                // Long questionId = Long.parseLong(record[]);
                // TODO
                /*Long questionId = 855L;
                
                if (i < 1000) {
                	questionId = 855L;
                } else if (i < 1500) {
                	questionId = 854L;
                } else if (i < 2900) {
                	questionId = 855L;
                } else {
                	questionId = 854L;
                }
                
                ++i;*/
                
                // System.out.println("Record [fpogx= " + fpogx + " , fpogy=" + fpogy + ", fpogId=" + fpogId + "]");
                
                DataRecordSimple drs = new DataRecordSimple();
                drs.setFpogx(fpogx);
                drs.setFpogy(fpogy);
                drs.setFpogs(fpogs);
                drs.setFpogd(fpogd);
                drs.setFpogId(fpogId);
                drs.setFpogv(fpogv);
                drs.setCnt(cnt);
                drs.setQuestionId(questionId);
                
                records.add(drs);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
		
        /*int i = 0;
        for(DataRecordSimple r: records) {
        	if (i % 100 == 0) {
        		System.out.println("=== " + i + " ===");
        	}
        	System.out.println(r.getQuestionId());
        	i++;
        }*/
        
		return records;
	}

	public TestingInfo getTestingInfoById(Long tiId) {
		return tiRepo.getOne(tiId);
	}

	public AnswerElement saveAnswerElement(AnswerElement a) {
		return null;
		//return answerElRepo.save(a);
	}

	public QuestionNew findNewQuestionById(Long questionId) {
		return qnRepo.getOne(questionId);
	}

	public AnswerElement findAnswerElementById(Long gaAnswerId) {
		return aeRepo.getOne(gaAnswerId);
	}
	
}
