package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
public @Data class MCTestAnswerDTO extends TestAnswerDTO {
	
	private List<Boolean> answer;
	
}
