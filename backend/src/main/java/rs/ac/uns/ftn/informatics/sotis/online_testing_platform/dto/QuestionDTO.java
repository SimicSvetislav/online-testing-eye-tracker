package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import java.util.List;

import lombok.Data;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.QuestionType;

public @Data class QuestionDTO {

	private Long id;
	private String title;
	private String text;
	
	private QuestionType type;
	
	private List<AnswerDTO> answers;
	
	private List<RegionDTO> regions;
	
}
