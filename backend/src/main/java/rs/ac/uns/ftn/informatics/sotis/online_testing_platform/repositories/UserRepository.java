package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	public User findByDisplayName(String displayName);
	
}
