package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
public @Data class Question {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String text;
	
	@ManyToOne
	private QuestionType type;
	
	@OneToMany(mappedBy = "question")
	private List<Answer> answers;
	
	@OneToMany
	private List<Region> regions; 
	
	@ManyToMany(mappedBy="questions")
	private List<Test> tests;
	
}
