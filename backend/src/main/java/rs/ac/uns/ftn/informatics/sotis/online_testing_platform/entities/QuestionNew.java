package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;
import rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto.RegionDTO;

@Entity
public @Data class QuestionNew {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<TitleElement> titleElements;
	@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<TextElement> textElements;
	@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<ImageElement> imageElements;
	@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<TextAreaElement> textAreaElements;
	@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<AnswerElement> answerElements;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private ButtonElement btnNextElement;
	@ManyToOne(cascade=CascadeType.ALL)
	private ButtonElement btnPreviousElement;
	
	@ManyToOne
	private QuestionType type;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Region> regions;
	
}
