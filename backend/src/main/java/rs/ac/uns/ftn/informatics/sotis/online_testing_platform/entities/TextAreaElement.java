package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@EqualsAndHashCode(callSuper = false)
@DiscriminatorValue("textArea")
public @Data class TextAreaElement extends BaseElement {

	private Integer redova;
	private Integer kolona;
	private String placeholder;
	
}
