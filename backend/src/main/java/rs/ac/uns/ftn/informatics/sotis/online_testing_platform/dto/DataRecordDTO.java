package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import lombok.Data;

public @Data class DataRecordDTO {
    // private Double mediaId;
    // private String mediaName;

    private Double cnt;
    private Double time;
    private Double timetick;

    private Double fpogx;
    private Double fpogy;
    private Double fpogs;
    private Double fpogd;
    private Double fpogId;
    private Double fpogv;

    private Double bpogx;
    private Double bpogy;
    private Double bpogv;

    private Double lpcx;
    private Double lpcy;
    private Double lpd;
    private Double lps;
    private Double lpv;

    private Double rpcx;
    private Double rpcy;
    private Double rpd;
    private Double rps;
    private Double rpv;
    
    private Double cx;
    private Double cy;
    private Double cs;
    
    private Integer questionId;

    // private String user;
    
    
    // private Integer questionId;
    
    @Override
    public String toString() {
        return 	cnt + "," + 
        		time + "," + 
        		timetick + "," +
        		
        		fpogx  + "," +
        		fpogy  + "," +
        		fpogs  + "," +
        		fpogd  + "," + 
        		fpogId + "," +
        		fpogv + "," +
        		
        		bpogx + "," +
        		bpogy + "," +
        		bpogv + "," +

        		lpcx + "," +
        		lpcy + "," +
        		lpd + "," +
        		lps + "," +
        		lpv + "," +

        		rpcx + "," +
        		rpcy + "," +
        		rpd + "," +
        		rps + "," +
        		rpv + "," +
        		
        		cx + "," +
        		cy + "," +
        		cs + "," +

				questionId;
        
        
        		// mediaId + "," + 
				// mediaName + "," +
				// user + "," +
        		// question + "," + 
        		
    }
    
}
