package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@EqualsAndHashCode(callSuper = false)
@DiscriminatorValue("text")
public @Data class TextElement extends BaseElement {
	
	@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<TextLine> textLines;
	
}
