package rs.ac.uns.ftn.informatics.sotis.online_testing_platform.dto;

import lombok.Data;

public @Data class SimpleReportDTO {

	private Long id;
	private String testName;
	private Integer correctAnswers;
	private Integer essayQuestions;
	private Integer questions;
	
}
