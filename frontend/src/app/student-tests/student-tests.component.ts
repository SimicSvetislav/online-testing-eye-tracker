import { TestSimpleDTO } from './../types/dtos/TestSimpleDTO';
import { TestDTO } from './../types/dtos/TestDTO';
import { StudentService } from './../services/student/student.service';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-tests',
  templateUrl: './student-tests.component.html',
  styleUrls: ['./student-tests.component.scss']
})
export class StudentTestsComponent implements OnInit {

  logged = false;
  userId: number;

  tests: TestSimpleDTO;

  constructor(private authService: AuthService, private router: Router,
              private studentService: StudentService) { }

  ngOnInit(): void {

    this.logged = this.authService.checkLoggedIn('ROLE_STUDENT');
    this.userId = this.authService.getUserId();

    this.studentService.getTestsNames().subscribe(data => {
      this.tests = data;
    });

  }

  navigate(path: string): boolean {

    this.router.navigateByUrl(path);

    return false;

  }

  logout(): void {
    this.authService.logOut();
    this.router.navigate(['/login']);
  }

  pass(event: Event): void {
    event.preventDefault();
  }

  startTest(testId: number): void {
    this.navigate('/student/tests/solve/' + testId);
  }

}
