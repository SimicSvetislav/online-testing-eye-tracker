import { TextLine } from './../types/elements/TextLine';
import { TeacherService } from './../services/teacher/teacher.service';
import { QuestionNewDTO } from '../types/dtos/QuestionNewDTO';
import { QuestionDTO } from './../types/dtos/QuestionDTO';
import { TestDTO } from './../types/dtos/TestDTO';
import { TextAreaElement } from './../types/elements/TextAreaElement';
import { TestAnswer } from './../types/TestAnswer';
import { ButtonElement } from './../types/elements/ButtonElement';
import { ImageElement } from './../types/elements/ImageElement';
import { AnswerElement } from './../types/elements/AnswerElement';
import { TextElement } from './../types/elements/TextElement';
import { TitleElement } from './../types/elements/TitleElement';
import { element } from 'protractor';
import { BaseElement } from './../types/elements/BaseElement';
import { Point } from './../types/Point';
import { RectangleP } from './../types/RectangleP';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { BinaryOperatorExpr } from '@angular/compiler';
import { type } from 'os';
import { Title } from '@angular/platform-browser';
import { Toast, ToastrService } from 'ngx-toastr';
import { QuestionType } from '../types/QuestionType';

@Component({
  selector: 'app-mark-regions',
  templateUrl: './mark-regions.component.html',
  styleUrls: ['./mark-regions.component.scss'],
})
export class MarkRegionsComponent implements OnInit {
  logged = false;
  userId: number;

  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;

  private ctx: CanvasRenderingContext2D;

  draw = false;
  origin = new Point();
  rectangles = new Array<RectangleP>();

  elements = new Array<BaseElement>();

  texts = new Array<TextElement>();
  images = new Array<ImageElement>();
  answers = new Array<AnswerElement>();
  titles = new Array<TitleElement>();
  textAreas = new Array<TextAreaElement>();

  btnNext: ButtonElement;
  btnPrevious: ButtonElement;

  constructor(
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private service: TeacherService
  ) {}

  ngOnInit(): void {
    this.logged = this.authService.checkLoggedIn('ROLE_TEACHER');
    this.userId = this.authService.getUserId();

    this.elements = JSON.parse(sessionStorage.getItem('elements')) as Array<
      BaseElement
    >;

    this.parseElements();

    this.initCanvas();

    const test = JSON.parse(sessionStorage.getItem('test')) as TestDTO;
    if (!(test.questionElements !== undefined && test.questionElements.length > 0)) {
      this.toastr.info('Please define regions of interest', 'Mark regions', {
        positionClass: 'toast-center-center',
        timeOut: 0,
        extendedTimeOut: 0,
      });
    }
  }

  navigate(path: string): boolean {
    this.router.navigateByUrl(path);

    return false;
  }

  preventDefault(event: Event): void {
    event.preventDefault();
  }

  parseElements(): void {
    this.elements.forEach((el) => {
      el.id = null;
      if ((el as AnswerElement).correct !== undefined) {
        this.answers.push(el as AnswerElement);
      } else if ((el as TitleElement).titleText !== undefined) {
        this.titles.push(el as TitleElement);
      } else if ((el as ImageElement).imgURL !== undefined) {
        this.images.push(el as ImageElement);
      } else if ((el as TextElement).lines !== undefined) {
        const tel = el as TextElement;
        tel.lines.forEach(line => {
          const l = new TextLine();
          l.text = line;
          tel.textLines.push(l);
        });
        this.texts.push(tel);
      } else if ((el as TextAreaElement).redova !== undefined) {
        this.textAreas.push(el as TextAreaElement);
      } else {
        const e = el as ButtonElement;
        const text = e.btnText;
        if (text === 'Next') {
          this.btnNext = e;
        } else if (text === 'Previous') {
          this.btnPrevious = e;
        }
      }
    });
  }

  initCanvas(): void {
    this.canvas.nativeElement.height = window.innerHeight;
    this.canvas.nativeElement.width = window.innerWidth;

    this.ctx = this.canvas.nativeElement.getContext('2d');

    this.ctx.strokeStyle = 'blue';
    this.ctx.fillStyle = 'yellow';
    this.ctx.lineWidth = 1;
  }

  canvas_click(event: MouseEvent): void {
    this.draw = true;

    this.origin.x = event.clientX;
    this.origin.y = event.clientY;
  }

  canvas_released(event: MouseEvent): void {
    if (this.draw) {
      // Korekcija ako kanvas nije u viewport-u
      const boundingRect = this.canvas.nativeElement.getBoundingClientRect();

      const xPercent = this.origin.x / window.innerWidth;
      const yPercent = this.origin.y / window.innerHeight;
      const widthPercent = (event.clientX - this.origin.x) / window.innerWidth;
      const heightPercent =
        (event.clientY - this.origin.y) / window.innerHeight;
      const newRect = new RectangleP(
        xPercent,
        yPercent,
        widthPercent,
        heightPercent
      );

      this.rectangles.push(newRect);

      this.draw = false;

      this.draw_rects();
    }
  }

  canvas_move(event: MouseEvent): void {
    if (this.draw) {
      // Nacrtaj postojece regione
      this.draw_rects();

      // Korekcija ako kanvas nije u viewport-u
      const boundingRect = this.canvas.nativeElement.getBoundingClientRect();

      // Nacrtaj region koji se trenutno crta
      this.draw_rect(
        this.origin.x - boundingRect.left,
        this.origin.y - boundingRect.top,
        event.clientX - this.origin.x,
        event.clientY - this.origin.y,
        0.1
      );
    }
  }
  canvas_leave(): void {
    if (this.draw) {
      this.draw = false;

      // Nacrtaj postojece regione
      this.draw_rects();
    }
  }

  private draw_rects(nums = false, fillAlpha = 0.05): void {
    this.clear_canvas();
    if (nums) {
      let i = 1;
      this.rectangles.forEach((rect) => {
        this.draw_rect_p(rect, fillAlpha, i++);
      });
    } else {
      this.rectangles.forEach((rect) => {
        this.draw_rect_p(rect, fillAlpha);
      });
    }
  }

  private draw_rect(
    x: number,
    y: number,
    width: number,
    height: number,
    fillAlpha: number,
    num?: number
  ): void {
    this.ctx.globalAlpha = 1.0;
    this.ctx.strokeRect(x, y, width, height);
    this.ctx.globalAlpha = fillAlpha;
    this.ctx.fillRect(x, y, width, height);
    this.ctx.globalAlpha = 1.0;

    if (num !== undefined) {
      this.ctx.fillStyle = 'black';
      this.ctx.fillText('' + num, x + width / 2, y + height / 2);
      this.ctx.fillStyle = 'yellow';
    }
  }

  private draw_rect_p(rect: RectangleP, fillAlpha: number, num?: number): void {
    const [x, y, width, height] = rect.toAbsolute();

    this.ctx.globalAlpha = 1.0;
    this.ctx.strokeRect(x, y, width, height);
    this.ctx.globalAlpha = fillAlpha;
    this.ctx.fillRect(x, y, width, height);
    this.ctx.globalAlpha = 1.0;

    if (num !== undefined) {
      this.ctx.fillStyle = 'black';
      this.ctx.fillText('' + num, x + width / 2, y + height / 2);
      this.ctx.fillStyle = 'yellow';
    }
  }

  clear_canvas(): void {
    this.ctx.clearRect(
      0,
      0,
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );
  }

  removeLastRegion(): void {
    const regionsNum = this.rectangles.length;
    if (regionsNum > 0) {
      this.rectangles.splice(regionsNum - 1, 1);
    }

    this.draw_rects();
  }

  removeAllRegions(): void {
    this.rectangles = new Array<RectangleP>();
    this.draw_rects();
  }

  normalizeRectangles(rectangles: Array<RectangleP>): Array<RectangleP> {
    const retVal = new Array<RectangleP>();

    rectangles.forEach((rect) => {
      if (rect.height > 0 && rect.width > 0) {
        retVal.push(rect);
      } else {
        const newRect = new RectangleP();

        if (rect.height < 0) {
          newRect.height = Math.abs(rect.height);
          newRect.y = rect.y + rect.height;
        } else {
          newRect.y = rect.y;
          newRect.height = rect.height;
        }

        if (rect.width < 0) {
          newRect.width = Math.abs(rect.width);
          newRect.x = rect.x + rect.width;
        } else {
          newRect.x = rect.x;
          newRect.width = rect.width;
        }

        retVal.push(newRect);
      }
    });

    return retVal;
  }

  finish(): void {
    /*if (this.rectangles.length === 0) {
      this.toastr.error('Please draw at least one region', 'No regions');
      return;
    }*/
  }

  finalizeQuestion(): TestDTO {
    const test = JSON.parse(sessionStorage.getItem('test')) as TestDTO;
    const question = new QuestionNewDTO();
    question.regions = this.normalizeRectangles(this.rectangles);

    const qt = new QuestionType();
    if (this.textAreas.length > 0) {
      qt.id = 2;
      qt.name = 'Essay question';
    } else {
      qt.id = 1;
      qt.name = 'Multiple choice question';
    }
    question.type = qt;

    question.answerElements = this.answers;
    question.titleElements = this.titles;
    question.textElements = this.texts;
    question.imageElements = this.images;
    question.textAreaElements = this.textAreas;

    question.btnNextElement = this.btnNext;
    question.btnPreviousElement = this.btnPrevious;

    test.questionElements.push(question);

    sessionStorage.setItem('test', JSON.stringify(test));

    return test;
  }

  newQuestion(): void {
    this.finalizeQuestion();

    this.navigate('/teacher/tests/new/question');
  }

  submitTest(): void {
    const test = this.finalizeQuestion();

    this.service.postNewTest(test, this.userId).subscribe((res) => {
      console.log('Test submitted');
      this.toastr.success(res);
      sessionStorage.removeItem('test');
      this.navigate('/teacher/tests');
    });

  }
}
