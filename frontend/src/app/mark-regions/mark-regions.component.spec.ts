import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkRegionsComponent } from './mark-regions.component';

describe('MarkRegionsComponent', () => {
  let component: MarkRegionsComponent;
  let fixture: ComponentFixture<MarkRegionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkRegionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkRegionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
