import { ReportDTO } from './../types/dtos/ReportDTO';
import { StudentService } from './../services/student/student.service';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-results',
  templateUrl: './student-results.component.html',
  styleUrls: ['./student-results.component.scss']
})
export class StudentResultsComponent implements OnInit {

  logged = false;
  userId: number;

  list = true;
  results: Array<ReportDTO>;
  report: ReportDTO;

  constructor(private authService: AuthService, private router: Router,
              private studentService: StudentService) { }

  ngOnInit(): void {

    this.logged = this.authService.checkLoggedIn('ROLE_STUDENT');
    this.userId = this.authService.getUserId();

    this.studentService.results(this.userId).subscribe(data => {
      this.results = data;
    });

  }

  navigate(path: string): boolean {
    this.router.navigateByUrl(path);

    return false;
  }

  logout(): void {
    this.authService.logOut();
    this.router.navigate(['/login']);
  }

  pass(event: Event): void {
    event.preventDefault();
  }

  viewReport(r: ReportDTO): void {
    this.list = false;
    this.report = r;
  }

  back(): void {
    this.list = true;
  }

}
