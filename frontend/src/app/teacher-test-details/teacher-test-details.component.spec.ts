import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherTestDetailsComponent } from './teacher-test-details.component';

describe('TeacherTestDetailsComponent', () => {
  let component: TeacherTestDetailsComponent;
  let fixture: ComponentFixture<TeacherTestDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherTestDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherTestDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
