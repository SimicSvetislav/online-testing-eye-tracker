import { QuestionNewDTO } from './../types/dtos/QuestionNewDTO';
import { TitleElement } from './../types/elements/TitleElement';
import { RectangleP } from './../types/RectangleP';
import { QuestionDTO } from './../types/dtos/QuestionDTO';
import { TestDTO } from './../types/dtos/TestDTO';
import { TeacherService } from './../services/teacher/teacher.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-teacher-test-details',
  templateUrl: './teacher-test-details.component.html',
  styleUrls: ['./teacher-test-details.component.scss']
})
export class TeacherTestDetailsComponent implements OnInit {

  logged = false;
  userId: number;

  testId: number;
  test = new TestDTO();

  list = true;
  qDetails = false;
  question = new QuestionDTO();
  questionNum: number;

  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;

  private ctx: CanvasRenderingContext2D;

  constructor(private authService: AuthService, private router: Router,
              private route: ActivatedRoute, private teacherService: TeacherService) { }

  ngOnInit(): void {

    this.logged = this.authService.checkLoggedIn('ROLE_TEACHER');
    this.userId = this.authService.getUserId();

    this.testId = this.route.snapshot.params.id;

    this.teacherService.getTeacherTestDetails(this.testId).subscribe(data => {
      this.test = data;
      // alert(this.test.name);
    });

    this.teacherService.getTeacherTestDetailsNew(this.testId).subscribe(data => {
      // data.questionElements[0].titleElements = new Array<TitleElement>();
      this.test = data;
      // alert(this.test.name);
    });

    this.initCanvas();
  }

  navigate(path: string): boolean {

    this.router.navigateByUrl(path);

    return false;

  }

  logout(): void {
    this.authService.logOut();
    this.router.navigate(['/login']);
  }

  login(): void {
    this.router.navigate(['/login']);
  }

  pass(event: Event): void {
    event.preventDefault();
  }

  initCanvas(): void {
    this.canvas.nativeElement.height = window.innerHeight * 0.93;
    this.canvas.nativeElement.width = window.innerWidth;

    this.ctx = this.canvas.nativeElement.getContext('2d');

    this.ctx.strokeStyle = 'blue';
    this.ctx.fillStyle = 'yellow';
    this.ctx.lineWidth = 1;
  }

  private draw_rects(fillAlpha = 0.05): void {
    this.question.regions.forEach((rect) => {
      this.draw_rect(
        rect,
        fillAlpha
      );
    });
  }

  private draw_rect(
    rect: RectangleP,
    fillAlpha: number,
    num?: number
  ): void {

    const [x, y, width, height] = this.toAbsolute(rect);

    this.ctx.globalAlpha = 1.0;
    this.ctx.strokeRect(x, y, width, height);
    this.ctx.globalAlpha = fillAlpha;
    this.ctx.fillRect(x, y, width, height);
    this.ctx.globalAlpha = 1.0;

    if (num !== undefined) {
      this.ctx.fillStyle = 'black';
      this.ctx.fillText('' + num, x + width / 2, y + height / 2);
      this.ctx.fillStyle = 'yellow';
    }
  }

  public toAbsolute(rect: RectangleP): Array<number> {

    const xAbs = rect.x * window.innerWidth;
    const yAbs = rect.y * window.innerHeight;

    const widthAbs = rect.width * window.innerWidth;
    const heightAbs = rect.height * window.innerHeight;

    return [xAbs, yAbs, widthAbs, heightAbs];
}

  showQuestion(q: QuestionNewDTO, qNum: number): void {
    // this.qDetails = true;
    // this.list = false;

    // this.question = q;
    // this.questionNum = qNum;

    // this.initCanvas();
    // this.draw_rects();

    sessionStorage.setItem('qDetails', JSON.stringify(q));
    this.navigate('/teacher/tests/details/' + this.testId + '/question');
  }

  back(): void {
    this.qDetails = false;
    this.list = true;

    this.question = new QuestionDTO();
  }

}
