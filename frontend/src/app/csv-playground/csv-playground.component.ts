import { User } from './../types/User';
import { DataRecord } from './../types/DataRecord';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-csv-playground',
  templateUrl: './csv-playground.component.html',
  styleUrls: ['./csv-playground.component.scss']
})
export class CsvPlaygroundComponent implements OnInit {

  records = new Array<DataRecord>();

  constructor() { }

  ngOnInit(): void {

    const startPoint = new Date();

    for (let i = 0; i < 216000; ++i) {
      const dr = new DataRecord();

      // dr.mediaId = 0;
      // dr.mediaName = 'NewMedia';

      dr.cnt = i;
      dr.time = new Date().getTime() - startPoint.getTime();
      dr.timetick = new Date().getTime();

      dr.fpogx = Math.random();
      dr.fpogy = Math.random();
      dr.fpogs = Math.random();
      dr.fpogd = Math.random();
      dr.fpogId = i;
      dr.fpogv = 1;

      dr.bpogx = Math.random();
      dr.bpogy = Math.random();
      dr.bpogv = 1;

      dr.cx = Math.random();
      dr.cy = Math.random();
      dr.cs = 0;

      // dr.user = undefined;

      this.records.push(dr);

      if (i % 10000 === 0) {
        // console.log(i);
      }

    }

    // alert(this.records.length);

  }

}
