import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsvPlaygroundComponent } from './csv-playground.component';

describe('CsvPlaygroundComponent', () => {
  let component: CsvPlaygroundComponent;
  let fixture: ComponentFixture<CsvPlaygroundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsvPlaygroundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsvPlaygroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
