import { RectangleP } from './../types/RectangleP';
import { AuthService } from './../services/auth/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Rectangle } from './../types/Rectangle';
import { Point } from '../types/Point';

@Component({
  selector: 'app-canvas-playground',
  templateUrl: './canvas-playground.component.html',
  styleUrls: ['./canvas-playground.component.scss'],
})
export class CanvasPlaygroundComponent implements OnInit {
  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;

  private ctx: CanvasRenderingContext2D;

  draw = false;
  origin = new Point();
  // rectangles = new Array<Rectangle>();
  rectangles = new Array<RectangleP>();

  logged = true;
  correct: boolean;

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    // this.canvas.nativeElement.height = window.innerHeight;
    // this.canvas.nativeElement.width = window.innerWidth;
    // this.canvas.nativeElement.height = 500;
    // this.canvas.nativeElement.width = 900;

    this.canvas.nativeElement.height = window.innerHeight * 0.93;
    this.canvas.nativeElement.width = window.innerWidth;

    // this.toastr.info(this.canvas.nativeElement.width + ' ' + this.canvas.nativeElement.height);

    this.ctx = this.canvas.nativeElement.getContext('2d');
    // this.ctx.strokeStyle = 'blue';
    // this.ctx.fillRect(0, 0, 5, 5);
    /*this.ctx.strokeRect(100, 100, 50, 50);
    this.ctx.strokeStyle = 'red';
    this.ctx.strokeRect(100, 100, -50, -50);
    this.ctx.strokeStyle = 'green';
    this.ctx.strokeRect(100, 100, -50, 50);
    this.ctx.strokeStyle = 'yellow';
    this.ctx.strokeRect(100, 100, 50, -50);*/

    this.ctx.strokeStyle = 'blue';
    this.ctx.fillStyle = 'yellow';
    this.ctx.lineWidth = 1;

    this.correct = false;

    this.toastr.info('Please select regions of interest', 'Region selection', {
      positionClass: 'toast-center-center',
      timeOut: 0,
      extendedTimeOut: 0,
    });
  }

  canvas_click(event: MouseEvent): void {
    // this.toastr.info(event.clientX + ' ' + event.clientY);
    this.draw = true;

    this.origin.x = event.clientX;
    this.origin.y = event.clientY;
  }

  canvas_released(event: MouseEvent): void {
    // this.toastr.info(event.clientX + ' ' + event.clientY);
    if (this.draw) {
      // Korekcija ako kanvas nije u viewport-u
      const boundingRect = this.canvas.nativeElement.getBoundingClientRect();

      /* const newRect = new Rectangle(new Point(this.origin.x - boundingRect.left, this.origin.y - boundingRect.top),
                                     new Point(event.clientX - boundingRect.left, event.clientY - boundingRect.top));

      this.rectangles.push(newRect);*/

      const xPercent = this.origin.x / window.innerWidth;
      const yPercent = this.origin.y / window.innerHeight;
      const widthPercent = (event.clientX - this.origin.x) / window.innerWidth;
      const heightPercent = (event.clientY - this.origin.y) / window.innerHeight;
      const newRect = new RectangleP(
        xPercent,
        yPercent,
        widthPercent,
        heightPercent
      );
      this.rectangles.push(newRect);

      this.draw = false;

      this.draw_rects();

      // this.toastr.info(this.canvas.nativeElement.height + '');
      // this.toastr.info(this.origin.x + '\n' + this.origin.y);
      // this.toastr.info((this.origin.x + boundingRect.left) + '\n' + (this.origin.y + boundingRect.top));
      // this.toastr.info(event.clientX + '\n' + event.clientY);
      // this.toastr.info(Math.round(event.clientX - boundingRect.left) + ' ' + Math.round(event.clientY - boundingRect.top));
      /*this.toastr.info((Math.round(event.clientX - boundingRect.left) /
                          this.canvas.nativeElement.width) + ' ' +
                        Math.round(event.clientY - boundingRect.top) /
                          this.canvas.nativeElement.height);*/
      // this.toastr.info(this.canvas.nativeElement.getBoundingClientRect().top + '');
      // this.toastr.info(window.innerHeight + '');
      // this.toastr.info(boundingRect.bottom + '');
    }
  }

  canvas_move(event: MouseEvent): void {
    if (this.draw) {
      // Nacrtaj postojece regione
      this.draw_rects();

      // Korekcija ako kanvas nije u viewport-u
      const boundingRect = this.canvas.nativeElement.getBoundingClientRect();

      // Nacrtaj region koji se trenutno crta
      this.draw_rect(
        this.origin.x - boundingRect.left,
        this.origin.y - boundingRect.top,
        event.clientX - this.origin.x,
        event.clientY - this.origin.y,
        0.1
      );
    }
  }

  canvas_leave(): void {
    if (this.draw) {
      this.draw = false;

      // Nacrtaj postojece regione
      this.draw_rects();
    }
  }

  private draw_rects(fillAlpha = 0.05): void {
    let i = 1;
    this.clear_canvas();
    this.rectangles.forEach((rect) => {
      this.draw_rect_p(rect, fillAlpha, i++);
    });
  }

  private draw_rect(
    x: number,
    y: number,
    width: number,
    height: number,
    fillAlpha: number,
    num?: number
  ): void {
    this.ctx.globalAlpha = 1.0;
    this.ctx.strokeRect(x, y, width, height);
    this.ctx.globalAlpha = fillAlpha;
    this.ctx.fillRect(x, y, width, height);
    this.ctx.globalAlpha = 1.0;

    if (num !== undefined) {
      this.ctx.fillStyle = 'black';
      this.ctx.fillText('' + num, x + width / 2, y + height / 2);
      this.ctx.fillStyle = 'yellow';
    }
  }

  private draw_rect_p(
    rect: RectangleP,
    fillAlpha: number,
    num?: number
  ): void {

    const [x, y, width, height] = rect.toAbsolute();

    this.ctx.globalAlpha = 1.0;
    this.ctx.strokeRect(x, y, width, height);
    this.ctx.globalAlpha = fillAlpha;
    this.ctx.fillRect(x, y, width, height);
    this.ctx.globalAlpha = 1.0;

    if (num !== undefined) {
      this.ctx.fillStyle = 'black';
      this.ctx.fillText('' + num, x + width / 2, y + height / 2);
      this.ctx.fillStyle = 'yellow';
    }
  }
  clear_canvas(): void {
    this.ctx.clearRect(
      0,
      0,
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );
  }

  navigate(path: string): boolean {
    this.router.navigateByUrl(path);

    return false;
  }

  logout(): void {
    this.authService.logOut();
    this.router.navigate(['/login']);
  }

  login(): void {
    this.router.navigate(['/login']);
  }

  pass(event: Event): void {
    event.preventDefault();
  }

  next(): void {
    alert(this.correct);
  }

  removeLastRegion(): void {
    const regionsNum = this.rectangles.length;
    if (regionsNum > 0) {
      this.rectangles.splice(regionsNum - 1, 1);
    }

    this.draw_rects();
  }

  removeAllRegions(): void {
    this.rectangles = new Array<RectangleP>();
    this.draw_rects();
  }

  nextRegions(): void {}
}
