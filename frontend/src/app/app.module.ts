import { ResponseInterceptor } from './response.interceptor';
import { TokenInterceptor } from './token.interceptor';
import { StudentProfileComponent } from './student-profile/student-profile.component';
import { TeacherProfileComponent } from './teacher-profile/teacher-profile.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { Routes, RouterModule } from '@angular/router';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { CanvasPlaygroundComponent } from './canvas-playground/canvas-playground.component';
import { TeacherTestsComponent } from './teacher-tests/teacher-tests.component';
import { TeacherResultsComponent } from './teacher-results/teacher-results.component';
import { StudentTestsComponent } from './student-tests/student-tests.component';
import { StudentResultsComponent } from './student-results/student-results.component';
import { TeacherTestDetailsComponent } from './teacher-test-details/teacher-test-details.component';
import { TeacherTestNewComponent } from './teacher-test-new/teacher-test-new.component';
import { StudentTestSolveComponent } from './student-test-solve/student-test-solve.component';
import { CsvPlaygroundComponent } from './csv-playground/csv-playground.component';
import { QuestionEditorComponent } from './question-editor/question-editor.component';

import { DndModule } from 'ngx-drag-drop';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { MarkRegionsComponent } from './mark-regions/mark-regions.component';
import { QuestionPreviewComponent } from './question-preview/question-preview.component';
import { StudentTestSolveNewComponent } from './student-test-solve-new/student-test-solve-new.component';
import { TeacherResultsRegionsComponent } from './teacher-results-regions/teacher-results-regions.component';


const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },

  { path: 'teacher/profile', component: TeacherProfileComponent },
  { path: 'teacher/tests', component: TeacherTestsComponent },
  { path: 'teacher/tests/new', component: TeacherTestNewComponent },
  { path: 'teacher/tests/new/question', component: QuestionEditorComponent },
  { path: 'teacher/tests/new/question/regions', component: MarkRegionsComponent },
  { path: 'teacher/tests/details/:id', component: TeacherTestDetailsComponent },
  { path: 'teacher/tests/details/:id/question', component: QuestionPreviewComponent},
  { path: 'teacher/results', component: TeacherResultsComponent },
  { path: 'teacher/results/regions', component: TeacherResultsRegionsComponent },

  { path: 'student/profile', component: StudentProfileComponent },
  { path: 'student/tests', component: StudentTestsComponent },
  { path: 'student/tests/solve/:id', component: StudentTestSolveNewComponent },
  { path: 'student/results', component: StudentResultsComponent },

  // { path: 'canvas', component: CanvasPlaygroundComponent },
  { path: 'csv', component: CsvPlaygroundComponent },

  { path: '', component: LoginComponent },

  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CanvasPlaygroundComponent,
    TeacherProfileComponent,
    StudentProfileComponent,
    NotFoundComponent,
    TeacherTestsComponent,
    TeacherResultsComponent,
    StudentTestsComponent,
    StudentResultsComponent,
    TeacherTestDetailsComponent,
    TeacherTestNewComponent,
    StudentTestSolveComponent,
    CsvPlaygroundComponent,
    QuestionEditorComponent,
    MarkRegionsComponent,
    QuestionPreviewComponent,
    StudentTestSolveNewComponent,
    TeacherResultsRegionsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    ),
    NgbModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 4000,
      closeButton: true,
      preventDuplicates: true,
      positionClass: 'toast-top-right',
    }),
    DndModule,
    DragDropModule,
    NgbModalModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResponseInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
