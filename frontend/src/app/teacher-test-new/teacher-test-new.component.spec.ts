import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherTestNewComponent } from './teacher-test-new.component';

describe('TeacherTestNewComponent', () => {
  let component: TeacherTestNewComponent;
  let fixture: ComponentFixture<TeacherTestNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherTestNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherTestNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
