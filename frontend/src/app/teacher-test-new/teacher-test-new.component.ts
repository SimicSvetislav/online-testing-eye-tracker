import { RectangleP } from './../types/RectangleP';
import { Rectangle } from './../types/Rectangle';
import { Point } from './../types/Point';
import { ToastrService } from 'ngx-toastr';
import { AnswerDTO } from './../types/dtos/AnswerDTO';
import { TeacherService } from './../services/teacher/teacher.service';
import { QuestionType } from './../types/QuestionType';
import { QuestionDTO } from './../types/dtos/QuestionDTO';
import { TestDTO } from './../types/dtos/TestDTO';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-teacher-test-new',
  templateUrl: './teacher-test-new.component.html',
  styleUrls: ['./teacher-test-new.component.scss'],
})
export class TeacherTestNewComponent implements OnInit {
  logged = false;
  userId: number;

  questionTypes: Array<QuestionType>;

  test = new TestDTO();

  question = new QuestionDTO();
  questionType: string;

  answers: Array<AnswerDTO>;
  answer: AnswerDTO;

  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;

  private ctx: CanvasRenderingContext2D;

  draw = false;
  origin = new Point();
  rectangles = new Array<RectangleP>();

  step1 = true;
  step2 = false;
  step22 = false;
  step3 = false;
  step4 = false;
  step5 = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private teacherService: TeacherService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.logged = this.authService.checkLoggedIn('ROLE_TEACHER');
    this.userId = this.authService.getUserId();

    this.test.questions = new Array<QuestionDTO>();
    this.questionType = '';

    this.teacherService.getQuestionTypes().subscribe((data) => {
      this.questionTypes = data;
    });

    this.initCanvas();

    sessionStorage.removeItem('test');

  }

  navigate(path: string): boolean {
    this.router.navigateByUrl(path);

    return false;
  }

  logout(): void {
    this.authService.logOut();
    this.router.navigate(['/login']);
  }

  login(): void {
    this.router.navigate(['/login']);
  }

  pass(event: Event): void {
    event.preventDefault();
  }

  hideAll(): void {
    this.step1 = false;
    this.step2 = false;
    this.step3 = false;
    this.step4 = false;
    this.step5 = false;
  }

  initCanvas(): void {
    this.canvas.nativeElement.height = window.innerHeight * 0.93;
    this.canvas.nativeElement.width = window.innerWidth;

    this.ctx = this.canvas.nativeElement.getContext('2d');

    this.ctx.strokeStyle = 'blue';
    this.ctx.fillStyle = 'yellow';
    this.ctx.lineWidth = 1;
  }

  submitStep1(): void {
    this.step1 = false;
    // this.step2 = true;
    // this.step22 = true;
    // alert(this.test.duration);

    sessionStorage.setItem('test', JSON.stringify(this.test));

    this.navigate('teacher/tests/new/question');
  }

  typeChange(): void {}

  submitStep2(): void {
    // alert(this.question.type);
    this.answers = new Array<AnswerDTO>();
    this.answer = new AnswerDTO();
    this.answer.correct = false;

    this.questionTypes.forEach((qt) => {
      if (+this.questionType === qt.id) {
        this.question.type = qt;
        this.questionType = '';
        return;
      }
    });

    this.step2 = false;

    if (+this.question.type.id === 1) {
      this.step3 = true;
    } else {
      this.initCanvas();
      this.step4 = true;
    }
  }

  addAnswer(f3: NgForm): void {
    // alert(this.answer.text);
    // alert(this.answer.correct);

    this.answers.push(this.answer);
    this.answer = new AnswerDTO();

    f3.reset();

    this.answer.correct = false;

    // alert(this.answers.length);
  }

  submitStep3(): void {
    // Ako je tip pitanaja multiple choice proveriti da li postoje barem dva ponudjena odgovora
    // Barem jedan odgovor mora biti tacan
    if (this.answers.length < 2) {
      // alert('Question must have at least two answers');
      this.toastr.error(
        'Question must have at least two answers',
        'Missing answers',
        { positionClass: 'toast-bottom-right' }
      );
      return;
    }

    if (!this.answers.some((a) => a.correct)) {
      this.toastr.error(
        'At least one answer must be correct',
        'Missing correct answer',
        { positionClass: 'toast-bottom-right' }
      );
      return;
    }

    this.question.answers = this.answers;
    this.answers = new Array<AnswerDTO>();

    this.step3 = false;
    this.step4 = true;

    this.initCanvas();

    this.toastr.info('Please select regions of interest', 'Region selection', {
      positionClass: 'toast-center-center',
      timeOut: 0,
      extendedTimeOut: 0,
    });
  }

  canvas_click(event: MouseEvent): void {
    this.draw = true;

    this.origin.x = event.clientX;
    this.origin.y = event.clientY;
  }

  canvas_released(event: MouseEvent): void {
    if (this.draw) {
      // Korekcija ako kanvas nije u viewport-u
      const boundingRect = this.canvas.nativeElement.getBoundingClientRect();

      const xPercent = this.origin.x / window.innerWidth;
      const yPercent = this.origin.y / window.innerHeight;
      const widthPercent = (event.clientX - this.origin.x) / window.innerWidth;
      const heightPercent =
        (event.clientY - this.origin.y) / window.innerHeight;
      const newRect = new RectangleP(
        xPercent,
        yPercent,
        widthPercent,
        heightPercent
      );

      this.rectangles.push(newRect);

      this.draw = false;

      this.draw_rects();
    }
  }

  canvas_move(event: MouseEvent): void {
    if (this.draw) {
      // Nacrtaj postojece regione
      this.draw_rects();

      // Korekcija ako kanvas nije u viewport-u
      const boundingRect = this.canvas.nativeElement.getBoundingClientRect();

      // Nacrtaj region koji se trenutno crta
      this.draw_rect(
        this.origin.x - boundingRect.left,
        this.origin.y - boundingRect.top,
        event.clientX - this.origin.x,
        event.clientY - this.origin.y,
        0.1
      );
    }
  }

  canvas_leave(): void {
    if (this.draw) {
      this.draw = false;

      // Nacrtaj postojece regione
      this.draw_rects();
    }
  }

  private draw_rects(nums = false, fillAlpha = 0.05): void {
    this.clear_canvas();
    if (nums) {
      let i = 1;
      this.rectangles.forEach((rect) => {
        this.draw_rect_p(rect, fillAlpha, i++);
      });
    } else {
      this.rectangles.forEach((rect) => {
        this.draw_rect_p(rect, fillAlpha);
      });
    }
  }

  private draw_rect(
    x: number,
    y: number,
    width: number,
    height: number,
    fillAlpha: number,
    num?: number
  ): void {
    this.ctx.globalAlpha = 1.0;
    this.ctx.strokeRect(x, y, width, height);
    this.ctx.globalAlpha = fillAlpha;
    this.ctx.fillRect(x, y, width, height);
    this.ctx.globalAlpha = 1.0;

    if (num !== undefined) {
      this.ctx.fillStyle = 'black';
      this.ctx.fillText('' + num, x + width / 2, y + height / 2);
      this.ctx.fillStyle = 'yellow';
    }
  }

  private draw_rect_p(rect: RectangleP, fillAlpha: number, num?: number): void {
    const [x, y, width, height] = rect.toAbsolute();

    this.ctx.globalAlpha = 1.0;
    this.ctx.strokeRect(x, y, width, height);
    this.ctx.globalAlpha = fillAlpha;
    this.ctx.fillRect(x, y, width, height);
    this.ctx.globalAlpha = 1.0;

    if (num !== undefined) {
      this.ctx.fillStyle = 'black';
      this.ctx.fillText('' + num, x + width / 2, y + height / 2);
      this.ctx.fillStyle = 'yellow';
    }
  }

  clear_canvas(): void {
    this.ctx.clearRect(
      0,
      0,
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );
  }

  removeLastRegion(): void {
    const regionsNum = this.rectangles.length;
    if (regionsNum > 0) {
      this.rectangles.splice(regionsNum - 1, 1);
    }

    this.draw_rects();
  }

  removeAllRegions(): void {
    this.rectangles = new Array<RectangleP>();
    this.draw_rects();
  }

  submitStep4(): void {
    // Proveriti da li postoji barem jedan region
    if (this.rectangles.length === 0) {
      this.toastr.error(
        'Question must have at least one region of interest',
        'No marked regions'
      );
      return;
    }

    this.step4 = false;
    this.step5 = true;

    this.question.regions = this.normalizeRectangles(this.rectangles);
    this.rectangles = new Array<RectangleP>();

    this.test.questions.push(this.question);
    this.question = new QuestionDTO();
  }

  normalizeRectangles(rectangles: Array<RectangleP>): Array<RectangleP> {
    const retVal = new Array<RectangleP>();

    rectangles.forEach((rect) => {
      if (rect.height > 0 && rect.width > 0) {
        retVal.push(rect);
      } else {
        const newRect = new RectangleP();

        if (rect.height < 0) {
          newRect.height = Math.abs(rect.height);
          newRect.y = rect.y + rect.height;
        } else {
          newRect.y = rect.y;
          newRect.height = rect.height;
        }

        if (rect.width < 0) {
          newRect.width = Math.abs(rect.width);
          newRect.x = rect.x + rect.width;
        } else {
          newRect.x = rect.x;
          newRect.width = rect.width;
        }

        retVal.push(newRect);
      }
    });

    return retVal;
  }

  newQuestion(): void {
    this.step5 = false;
    this.step2 = true;
  }

  submitTest(): void {
    if (this.test.questions.length === 0) {
      this.toastr.error('Test must have at least one question', 'No questions');
      return;
    }

    this.teacherService
      .postTest(this.test, this.userId)
      .subscribe((response) => {
        this.toastr.success(response);
        this.navigate('/teacher/tests');
      });
  }

  removeAnswer(answer: AnswerDTO): void {
    const i = this.answers.indexOf(answer);
    this.answers.splice(i, 1);
  }
}
