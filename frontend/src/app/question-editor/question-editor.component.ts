import { TestDTO } from './../types/dtos/TestDTO';
import { TextAreaElement } from './../types/elements/TextAreaElement';
import { ButtonElement } from './../types/elements/ButtonElement';
import { BaseElement } from './../types/elements/BaseElement';
import { AuthService } from './../services/auth/auth.service';
import { TitleElement } from './../types/elements/TitleElement';
import { ImageElement } from './../types/elements/ImageElement';
import { ToastrService } from 'ngx-toastr';
import { AnswerElement } from './../types/elements/AnswerElement';
import { TextElement } from './../types/elements/TextElement';
import { CdkDrag, CdkDragDrop, CdkDragEnd } from '@angular/cdk/drag-drop';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Form, NgForm } from '@angular/forms';
import { NgbModalWindow } from '@ng-bootstrap/ng-bootstrap/modal/modal-window';
import { NavigationEnd, Router } from '@angular/router';
import { setServers } from 'dns';

@Component({
  selector: 'app-question-editor',
  templateUrl: './question-editor.component.html',
  styleUrls: ['./question-editor.component.scss'],
})
export class QuestionEditorComponent implements OnInit {
  logged = false;
  userId: number;

  texts = new Array<TextElement>();
  items = new Array<string>();
  images = new Array<ImageElement>();
  answers = new Array<AnswerElement>();
  titles = new Array<TitleElement>();
  textAreas = new Array<TextAreaElement>();

  titleEl = new TitleElement();
  questionTxt: string;
  answerElement = new AnswerElement();
  imageElement = new ImageElement();
  textAreaElement = new TextAreaElement();

  titleModalView = true;
  textModalView = true;
  answerModalView = true;
  imageModalView = true;
  textAreaModalView = true;

  chooseImageText = 'Choose image';

  show = new Map<number, boolean>();

  counter = 1;

  textInitPosTop = 0.1;
  textInitPosLeft = 0.3;

  titleInitPosTop = 0.01;
  titleInitPosLeft = 0.15;

  answerInitPosTop = 0.4;
  answerInitPosLeft = 0.4;

  imgInitPosTop = 0.05;
  imgInitPosLeft = 0.1;

  textAreaInitPosLeft = 0.2;
  textAreaInitPosTop = 0.2;

  btnNext = new ButtonElement();
  btnPrevious = new ButtonElement();

  btnNextInitPosTop = 0.9;
  btnNextInitPosLeft = 0.1;

  btnPreviousInitPosTop = 0.9;
  btnPreviousInitPosLeft = 0.9;

  @ViewChild('addTitleModal') public titleModal: NgbModalWindow;

  constructor(
    private toastr: ToastrService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.logged = this.authService.checkLoggedIn('ROLE_TEACHER');
    this.userId = this.authService.getUserId();

    // this.container.nativeElement.height = window.innerHeight;
    // this.container.nativeElement.width = window.innerWidth;
    // this.items.push('ITEM 1');

    this.btnNext.btnText = 'Next';
    this.btnNext.levo = this.btnNextInitPosLeft;
    this.btnNext.top = this.btnNextInitPosTop;

    this.btnPrevious.btnText = 'Previous';
    this.btnPrevious.levo = this.btnPreviousInitPosLeft;
    this.btnPrevious.top = this.btnPreviousInitPosTop;

    const testSession = JSON.parse(sessionStorage.getItem('test')) as TestDTO;
    if (!(testSession.questionElements !== undefined && testSession.questionElements.length > 0)) {
      this.toastr.info('Please define question elements', 'New question', {
        positionClass: 'toast-center-center',
        timeOut: 0,
        extendedTimeOut: 0,
      });
    }

    const test = JSON.parse(sessionStorage.getItem('test')) as TestDTO;
  }

  navigate(path: string): boolean {
    this.router.navigateByUrl(path);

    return false;
  }

  onDragEnded(event: CdkDragEnd): void {
    const element = event.source.getRootElement();
    const boundingClientRect = element.getBoundingClientRect();
    // const parentPosition = this.getPosition(element);
    console.log('x: ' + boundingClientRect.x, 'y: ' + boundingClientRect.y);
  }

  onDragEndedTitle(event: CdkDragEnd, id: number): void {
    const element = event.source.getRootElement();
    const boundingClientRect = element.getBoundingClientRect();

    const te = this.titles.find((t) => t.id === id);
    te.levo = boundingClientRect.x / window.innerWidth;
    te.top = boundingClientRect.y / window.innerHeight;

    // this.toastr.info(te.top + ' ' + te.left);
  }

  onDragEndedText(event: CdkDragEnd, id: number): void {
    const element = event.source.getRootElement();
    const boundingClientRect = element.getBoundingClientRect();

    const te = this.texts.find((t) => t.id === id);
    te.levo = boundingClientRect.x / window.innerWidth;
    te.top = boundingClientRect.y / window.innerHeight;

    // this.toastr.info(te.top + ' ' + te.left);
  }

  onDragEndedImage(event: CdkDragEnd, id: number): void {
    const element = event.source.getRootElement();
    const boundingClientRect = element.getBoundingClientRect();

    const ie = this.images.find((t) => t.id === id);
    ie.levo = boundingClientRect.x / window.innerWidth;
    ie.top = boundingClientRect.y / window.innerHeight;

    // this.toastr.info(ie.top + ' ' + ie.left);
  }

  onDragEndedNext(event: CdkDragEnd): void {
    const element = event.source.getRootElement();
    const boundingClientRect = element.getBoundingClientRect();

    this.btnNext.levo = boundingClientRect.x / window.innerWidth;
    this.btnNext.top = boundingClientRect.y / window.innerHeight;

    // this.toastr.info(ie.top + ' ' + ie.left);
  }

  onDragEndedPrevious(event: CdkDragEnd): void {
    const element = event.source.getRootElement();
    const boundingClientRect = element.getBoundingClientRect();

    this.btnPrevious.levo = boundingClientRect.x / window.innerWidth;
    this.btnPrevious.top = boundingClientRect.y / window.innerHeight;

    // this.toastr.info(ie.top + ' ' + ie.left);
  }

  onDragEndedAnswer(event: CdkDragEnd, id: number): void {
    const element = event.source.getRootElement();
    const boundingClientRect = element.getBoundingClientRect();

    const ae = this.answers.find((t) => t.id === id);
    ae.levo = boundingClientRect.x / window.innerWidth;
    ae.top = boundingClientRect.y / window.innerHeight;

    // this.toastr.info(ae.top + ' ' + ae.left);
  }

  onDragEndedTextArea(event: CdkDragEnd, id: number): void {
    const element = event.source.getRootElement();
    const boundingClientRect = element.getBoundingClientRect();

    const tae = this.textAreas.find((t) => t.id === id);
    tae.levo = boundingClientRect.x / window.innerWidth;
    tae.top = boundingClientRect.y / window.innerHeight;
  }

  getPosition(el: HTMLElement): Position {
    let x = 0;
    let y = 0;
    while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
      x += el.offsetLeft - el.scrollLeft;
      y += el.offsetTop - el.scrollTop;
      el = el.offsetParent as HTMLElement;
    }
    return { top: y, left: x };
  }

  add(): void {
    console.log('1');
  }

  addText(form: NgForm): void {
    // this.items.push('ITEM ' + (this.items.length + 1));
    const te = new TextElement();
    te.id = this.counter++;
    te.lines = this.questionTxt.split('\n');
    // te.left = window.innerWidth * this.textInitPosLeft;
    // te.top = window.innerHeight * this.textInitPosTop;

    te.levo = this.textInitPosLeft;
    te.top = this.textInitPosTop;

    this.texts.push(te);

    this.questionTxt = '';
    const fieldId = 'questionText';
    form.controls[fieldId].markAsUntouched();
    form.controls[fieldId].markAsPristine();
    this.textModalView = false;
  }

  chosenImage(event): void {
    const imgFile = event.target.files[0];

    const fileType = imgFile.type;
    const contains: boolean = fileType.includes('image');
    if (!contains) {
      this.toastr.warning('Selected file isn\'t an image!', 'Wrong file type');
      return;
    }

    this.chooseImageText = imgFile.name;

    const reader = new FileReader();

    reader.readAsDataURL(imgFile);
    reader.onload = (e) => {
      this.imageElement.imgURL = reader.result;
    };
  }

  addImage(form: NgForm, event: Event): void {
    this.imageElement.id = this.counter++;
    // this.imageElement.left = window.innerWidth * this.imgInitPosLeft;
    // this.imageElement.top = window.innerHeight * this.imgInitPosTop;
    this.imageElement.levo = this.imgInitPosLeft;
    this.imageElement.top = this.imgInitPosTop;
    this.images.push(this.imageElement);

    this.imageElement = new ImageElement();

    this.imageModalView = false;
  }

  addAnswer(form: NgForm): void {
    this.answerElement.id = this.counter++;
    // this.answerElement.left = window.innerWidth * this.answerInitPosLeft;
    // this.answerElement.top = window.innerHeight * this.answerInitPosTop;
    this.answerElement.levo = this.answerInitPosLeft;
    this.answerElement.top = this.answerInitPosTop;
    this.answers.push(this.answerElement);
    // alert(this.answerElement.correct)

    this.answerElement = new AnswerElement();
    const fieldId = 'questionAnswer';
    form.controls[fieldId].markAsUntouched();
    form.controls[fieldId].markAsPristine();
    this.answerModalView = false;
  }

  addTitle(form: NgForm): void {
    this.titleEl.id = this.counter++;
    // this.titleEl.left = window.innerWidth * this.titleInitPosLeft;
    // this.titleEl.top = window.innerHeight * this.titleInitPosTop;
    this.titleEl.levo = this.titleInitPosLeft;
    this.titleEl.top = this.titleInitPosTop;
    this.titles.push(this.titleEl);

    this.titleEl = new TitleElement();
    const fieldId = 'questionTitle';
    form.controls[fieldId].markAsUntouched();
    form.controls[fieldId].markAsPristine();
    this.titleModalView = false;
  }

  addTextArea(form: NgForm): void {
    this.textAreaElement.id = this.counter++;
    // this.textAreaElement.left = window.innerWidth * this.textAreaInitPosLeft;
    // this.textAreaElement.top = window.innerHeight * this.textAreaInitPosTop;
    this.textAreaElement.levo = this.textAreaInitPosLeft;
    this.textAreaElement.top = this.textAreaInitPosTop;

    this.textAreas.push(this.textAreaElement);

    this.textAreaElement = new TextAreaElement();

    this.textAreaModalView = false;
  }

  preventDefault(event: Event): void {
    event.preventDefault();
  }

  removeTitle(title: TitleElement): void {
    this.removeArrayElement(this.titles, title);
  }

  removeText(te: TextElement): void {
    this.removeArrayElement(this.texts, te);
  }

  removeImage(ie: ImageElement): void {
    this.removeArrayElement(this.images, ie);
  }

  removeAnswer(ae: AnswerElement): void {
    this.removeArrayElement(this.answers, ae);
  }

  removeTextArea(tae: TextAreaElement): void {
    this.removeArrayElement(this.textAreas, tae);
  }

  removeArrayElement(arr: Array<any>, el: object): boolean {
    const index = arr.indexOf(el, 0);
    if (index > -1) {
      arr.splice(index, 1);
      return true;
    }

    return false;
  }

  afterHidden(event: Event): void {
    alert('a');
    this.titleEl = new TitleElement();
    this.questionTxt = '';
    this.answerElement = new AnswerElement();
    this.imageElement = new ImageElement();
  }

  finish(): void {
    if (!this.validate()) {
      return;
    }

    const elements = new Array<BaseElement>();

    this.titles.forEach((el) => {
      // el.left = el.left / window.innerWidth;
      // el.top = el.top / window.innerHeight;
      elements.push(el);
    });

    this.texts.forEach((el) => {
      // el.left = el.left / window.innerWidth;
      // el.top = el.top / window.innerHeight;
      elements.push(el);
    });

    this.images.forEach((el) => {
      // el.left = el.left / window.innerWidth;
      // el.top = el.top / window.innerHeight;
      elements.push(el);
    });

    this.answers.forEach((el) => {
      // el.left = el.left / window.innerWidth;
      // el.top = el.top / window.innerHeight;
      elements.push(el);
    });

    this.textAreas.forEach((el) => {
      elements.push(el);
    });

    elements.push(this.btnNext);
    elements.push(this.btnPrevious);

    sessionStorage.setItem('elements', JSON.stringify(elements));

    this.navigate('/teacher/tests/new/question/regions');
  }

  validate(): boolean {
    if (this.texts.length === 0) {
      this.toastr.error('Text not added', 'Can\'t proceed');
      return false;
    }

    if (this.answers.length < 2 && this.textAreas.length === 0) {
      this.toastr.error('At least two answers or one text area must be added', 'Can\'t proceed');
      return false;
    }

    if (this.answers.length > 0 && this.answers.find((a) => a.correct) === undefined) {
      this.toastr.error('At least one answer must be true', 'Can\'t proceed');
      return false;
    }

    return true;
  }
}

class Position {
  top: number;
  left: number;
}
