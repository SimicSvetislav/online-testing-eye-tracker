import { RectangleP } from './../types/RectangleP';
import { TeacherService } from './../services/teacher/teacher.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { QuestionNewDTO } from './../types/dtos/QuestionNewDTO';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-question-preview',
  templateUrl: './question-preview.component.html',
  styleUrls: ['./question-preview.component.scss'],
})
export class QuestionPreviewComponent implements OnInit {
  logged = false;
  userId: number;

  testId: number;

  question: QuestionNewDTO;

  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;

  private ctx: CanvasRenderingContext2D;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  color = false;
  regions = false;

  ngOnInit(): void {
    this.logged = this.authService.checkLoggedIn('ROLE_TEACHER');
    this.userId = this.authService.getUserId();

    this.testId = this.route.snapshot.params.id;

    this.question = JSON.parse(sessionStorage.getItem('qDetails'));

    this.initCanvas();

    if (this.regions) {
      this.draw_rects();
    }
  }

  navigate(path: string): boolean {
    this.router.navigateByUrl(path);

    return false;
  }

  initCanvas(): void {
    this.canvas.nativeElement.height = window.innerHeight;
    this.canvas.nativeElement.width = window.innerWidth;

    this.ctx = this.canvas.nativeElement.getContext('2d');

    this.ctx.strokeStyle = 'blue';
    this.ctx.fillStyle = 'yellow';
    this.ctx.lineWidth = 1;
  }

  private draw_rects(fillAlpha = 0.05): void {
    this.question.regions.forEach((rect) => {
      this.draw_rect(rect, fillAlpha);
    });
  }

  private draw_rect(rect: RectangleP, fillAlpha: number, num?: number): void {
    const [x, y, width, height] = this.toAbsolute(rect);

    this.ctx.globalAlpha = 1.0;
    this.ctx.strokeRect(x, y, width, height);
    this.ctx.globalAlpha = fillAlpha;
    this.ctx.fillRect(x, y, width, height);
    this.ctx.globalAlpha = 1.0;

    if (num !== undefined) {
      this.ctx.fillStyle = 'black';
      this.ctx.fillText('' + num, x + width / 2, y + height / 2);
      this.ctx.fillStyle = 'yellow';
    }
  }

  public toAbsolute(rect: RectangleP): Array<number> {
    const xAbs = rect.x * window.innerWidth;
    const yAbs = rect.y * window.innerHeight;

    const widthAbs = rect.width * window.innerWidth;
    const heightAbs = rect.height * window.innerHeight;

    return [xAbs, yAbs, widthAbs, heightAbs];
  }

  clear_canvas(): void {
    this.ctx.clearRect(
      0,
      0,
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );
  }

  preventDefault(event: Event): void {
    event.preventDefault();
  }

  toggleRegions(): void {
    this.regions = !this.regions;

    if (this.regions) {
      this.draw_rects();
    } else {
      this.clear_canvas();
    }
  }

  back(): void {
    this.navigate('/teacher/tests/details/' + this.testId);
  }
}
