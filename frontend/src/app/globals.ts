export const API = 'http://localhost:8080/api/';
export const API_TEACHER = 'http://localhost:8080/api/teacher/';
export const API_STUDENT = 'http://localhost:8080/api/student/';

export const PROXY = 'http://localhost:7070/api/';
