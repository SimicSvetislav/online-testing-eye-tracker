import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentTestSolveComponent } from './student-test-solve.component';

describe('StudentTestSolveComponent', () => {
  let component: StudentTestSolveComponent;
  let fixture: ComponentFixture<StudentTestSolveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentTestSolveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentTestSolveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
