import { ProxyService } from './../services/proxy/proxy.service';
import { MockService } from './../services/mock/mock.service';
import { DataRecord } from './../types/DataRecord';
import { SimpleReportDTO } from './../types/dtos/SimpleReportDTO';
import { IdAnswerPair } from './../types/IdAnswerPair';
import { ToastrService } from 'ngx-toastr';
import { TestingInfo } from './../types/TestingInfo';
import { EssayTestAnswer } from './../types/EssayTestAnswer';
import { MCTestAnswer } from './../types/MCTestAnswer';
import { TestAnswer } from './../types/TestAnswer';
import { QuestionDTO } from './../types/dtos/QuestionDTO';
import { TestDTO } from './../types/dtos/TestDTO';
import { StudentService } from './../services/student/student.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';

@Component({
  selector: 'app-student-test-solve',
  templateUrl: './student-test-solve.component.html',
  styleUrls: ['./student-test-solve.component.scss'],
})
export class StudentTestSolveComponent implements OnInit, OnDestroy {
  logged = false;
  userId: number;

  testId: number;
  test = new TestDTO();

  currentQuestion = new QuestionDTO();
  questionCounter: number;
  testAnswers = new Array<TestAnswer>();

  simpleReport: SimpleReportDTO;
  phaseSolving = true;

  records = new Array<DataRecord>();

  wait: boolean;
  essayQuestionsExist = false;
  // generateData = false;

  values = new Array<number>();
  startPoint: Date;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private studentService: StudentService,
    private toastr: ToastrService,
    private mockService: MockService,
    private proxyService: ProxyService
  ) {}

  ngOnDestroy(): void {

    if (this.phaseSolving) {
      alert('Attempt will be recorded!');
      this.finishTest();
    }

    this.proxyService.stop().subscribe(res => {
      console.log(res);
    });
  }

  ngOnInit(): void {
    this.logged = this.authService.checkLoggedIn('ROLE_STUDENT');
    this.userId = this.authService.getUserId();

    this.testId = this.route.snapshot.params.id;

    this.studentService.getTest(this.testId).subscribe((data) => {
      this.test = data;
      this.currentQuestion = this.test.questions[0];
      this.questionCounter = 1;
      // this.generateData = true;
      // this.asyncFun();
      // this.testAnswers = new Array(this.test.questions.length);
      this.test.questions.forEach((q) => {
        if (q.type.id === 1) {
          const idAnswerPairs = new Array<IdAnswerPair>();
          for (const answ of q.answers) {
            const idAnswerPair = new IdAnswerPair();
            idAnswerPair.givenAnswer = false;
            idAnswerPair.answerId = answ.id;
            idAnswerPairs.push(idAnswerPair);
          }

          const mcTestAnswer = new MCTestAnswer();
          mcTestAnswer.questionId = q.id;
          mcTestAnswer.answer = idAnswerPairs;

          this.testAnswers.push(mcTestAnswer);
        } else if (q.type.id === 2) {
          this.essayQuestionsExist = true;
          const essayTestAnswer = new EssayTestAnswer();
          essayTestAnswer.questionId = q.id;
          essayTestAnswer.answer = '';

          this.testAnswers.push(essayTestAnswer);
        }
      });

      this.proxyService.start(this.currentQuestion.id).subscribe((res) => {
        console.log(res);
      });

      // alert(this.test.questions[0].type.id);
      // alert(this.testAnswers.length);
    });

    // this.startWebWorker();

    this.startPoint = new Date();
  }

  /*async asyncFun(): Promise<void> {
    let i = 1;
    while (this.generateData) {
      const value = await this.generateRecord(i++) as DataRecord;
      value.question = this.questionCounter;
      value.questionId = this.currentQuestion.id;
      this.records.push(value);
      // console.log(`async result: ${value}`);
    }
  }*/

  /*generateRecord(i): Promise<DataRecord> {
    return new Promise(resolve => {
      setTimeout(() => {
        const record = this.mockService.generateRecord(this.startPoint, i);
        resolve(record);
      }, 17);
    });
  }*/

  navigate(path: string): boolean {

    if (this.phaseSolving) {
      return false;
    }

    this.proxyService.stop().subscribe(res => {
      console.log(res);
    });
    this.router.navigateByUrl(path);
    return false;
  }

  logout(): void {

    if (this.phaseSolving) {
      return;
    }

    this.proxyService.stop().subscribe(res => {
      console.log(res);
    });
    this.authService.logOut();
    this.router.navigate(['/login']);
  }

  pass(event: Event): void {
    event.preventDefault();
  }

  nextQuestion(): void {
    this.currentQuestion = this.test.questions[this.questionCounter++];
    this.updateQuestionProxy();
  }

  previousQuestion(): void {
    this.questionCounter--;
    this.currentQuestion = this.test.questions[this.questionCounter - 1];
    this.updateQuestionProxy();
  }

  updateQuestionProxy(): void {
    this.proxyService.question(this.currentQuestion.id).subscribe((res) => {
      console.log(res);
    });
  }

  finishTest(): void {
    // this.generateData = false;
    // alert(this.values.length);
    this.phaseSolving = false;
    this.wait = true;

    const testingInfo = new TestingInfo();

    testingInfo.studentId = this.userId;
    testingInfo.testId = this.testId;
    testingInfo.testAnswers = this.testAnswers;

    // this.records = this.mockService.generateEyeTrackerData();

    this.proxyService.finish().subscribe((data) => {
      // this.records = JSON.parse(data);
      this.records = data as Array<DataRecord>;
      this.studentService.submitTest(testingInfo).subscribe((report) => {
        // this.toastr.success('Test successfully submitted');
        this.simpleReport = report;
        this.wait = false;
        this.studentService
          .submitEyeTrackerRecords(this.records, this.simpleReport.id)
          .subscribe((response) => {
            console.log(response);
          });
      });
    });

    // alert(this.testAnswers);
  }

  close(): void {
    this.navigate('/student/tests');
  }

  startWebWorker(): void {
    if (typeof Worker !== 'undefined') {
      // Create a new
      const worker = new Worker('./eye-tracker-worker.worker.ts', {
        type: 'module',
      });
      worker.onmessage = ({ data }) => {
        console.log(`page got message: ${data}`);
      };
      worker.postMessage(42);
    } else {
      // Web Workers are not supported in this environment.
      // You should add a fallback so that your program still executes correctly.
    }
  }

  @HostListener('window:beforeunload', ['$event'])
  handleBeforeUnload($event: any): void {
  if (this.phaseSolving) {
      $event.preventDefault();
      // alert('Current answers will be recorder');
      this.finishTest();
  }
}
}


