import { ReportAnswerDTO } from './../types/dtos/ReportAnswerDTO';
import { TestDTO } from './../types/dtos/TestDTO';
import { RectangleP } from './../types/RectangleP';
import { TeacherService } from './../services/teacher/teacher.service';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { QuestionNewDTO } from './../types/dtos/QuestionNewDTO';
import { ReportDTO } from './../types/dtos/ReportDTO';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-teacher-results-regions',
  templateUrl: './teacher-results-regions.component.html',
  styleUrls: ['./teacher-results-regions.component.scss'],
})
export class TeacherResultsRegionsComponent implements OnInit {
  logged = false;
  userId: number;

  report = new ReportDTO();

  questionNum: number;
  currentQuestion = new QuestionNewDTO();
  // regionID -> index
  regionMapping = new Map<number, number>();

  regionShowBools: Array<boolean>;
  regionShowTextArea: string;

  regionsSequenceInfo: Array<string>;

  answerReport: ReportAnswerDTO;

  test: TestDTO;

  color = false;

  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;

  private ctx: CanvasRenderingContext2D;

  constructor(
    private authService: AuthService,
    private router: Router,
    private teacherService: TeacherService
  ) {}

  ngOnInit(): void {
    this.logged = this.authService.checkLoggedIn('ROLE_TEACHER');
    this.userId = this.authService.getUserId();

    this.report = JSON.parse(sessionStorage.getItem('report'));
    this.questionNum = 1;

    this.regionShowBools = new Array<boolean>();
    this.regionShowTextArea = '';

    this.initCanvas();

    this.teacherService.getTeacherTestDetailsNew(this.report.testId).subscribe((data) => {
      this.test = data;
      this.currentQuestion = this.test.questionElements[this.questionNum - 1];

      this.currentQuestion.regions.forEach((region, index) => {
        this.regionMapping.set(region.id, index + 1);
      });

      this.teacherService.getResultAnswer(this.report.id).subscribe((res) => {
        this.answerReport = res as ReportAnswerDTO;
        this.prepareAnswers();
        this.fillRegionsInfoString();
      });
      this.draw_rects();
    });

  }

  prepareAnswers(): void {
    this.regionShowBools = new Array<boolean>();
    this.regionShowTextArea = '';
    if (this.currentQuestion.type.id === 1) {
      this.currentQuestion.answerElements.forEach((el) => {
        const checked = this.answerReport.qreports[
          this.questionNum - 1
        ].answered.includes(el.text);
        this.regionShowBools.push(checked);
      });
    } else if (this.currentQuestion.type.id === 2) {
      this.regionShowTextArea = this.answerReport.qreports[
        this.questionNum - 1
      ].answered[0];
    }
  }

  previousQuestion(): void {
    this.questionNum--;

    this.currentQuestion = this.test.questionElements[this.questionNum - 1];

    this.currentQuestion.regions.forEach((region, index) => {
      this.regionMapping.set(region.id, index + 1);
    });

    this.fillRegionsInfoString();

    this.teacherService.getResultAnswer(this.report.id).subscribe((res) => {
      this.answerReport = res as ReportAnswerDTO;
      this.prepareAnswers();
    });

    this.clear_canvas();
    this.draw_rects();
  }

  nextQuestion(): void {
    this.questionNum++;

    this.currentQuestion = this.test.questionElements[this.questionNum - 1];

    this.currentQuestion.regions.forEach((region, index) => {
      this.regionMapping.set(region.id, index + 1);
    });

    this.fillRegionsInfoString();

    this.teacherService.getResultAnswer(this.report.id).subscribe((res) => {
      this.answerReport = res as ReportAnswerDTO;
      this.prepareAnswers();
    });

    this.clear_canvas();
    this.draw_rects();
  }

  fillRegionsInfoString(): void {
    this.regionsSequenceInfo = new Array<string>();
    this.answerReport.qrs.forEach((s, index) => {
      if (s.questionId === this.currentQuestion.id) {
        let str = s.regionSequence.filter((seq) => seq.length > 0).toString();
        const splitted = str.split(',');
        const mappedIds = new Array<number>();
        splitted.forEach((spl) => {
          mappedIds.push(this.regionMapping.get(+spl));
        });
        str = mappedIds.join(' -> ');
        this.regionsSequenceInfo.push(str);
      }
    });
  }

  getCSVFile(): void {
    this.teacherService.getCSVFile(this.report.id).subscribe((data) => {
      const blob = new Blob([data], { type: 'text/csv' });
      const filename =
        this.report.studentIndex +
        '_' +
        this.report.attempt +
        '_' +
        this.report.testName;
      saveAs(blob, filename + '.csv');
      // alert(data);
    });
  }

  back(): void {
    this.navigate('teacher/results');
    this.report = new ReportDTO();
  }

  initCanvas(): void {
    this.canvas.nativeElement.height = window.innerHeight;
    this.canvas.nativeElement.width = window.innerWidth;

    this.ctx = this.canvas.nativeElement.getContext('2d');

    this.ctx.strokeStyle = 'blue';
    this.ctx.fillStyle = 'yellow';
    this.ctx.lineWidth = 1;
    this.ctx.font = '18px Arial';
  }

  private draw_rects(fillAlpha = 0.05): void {
    this.currentQuestion.regions.forEach((rect) => {
      this.draw_rect(rect, fillAlpha, this.regionMapping.get(rect.id));
    });
  }

  private draw_rect(rect: RectangleP, fillAlpha: number, num?: number): void {
    const [x, y, width, height] = this.toAbsolute(rect);

    this.ctx.globalAlpha = 1.0;
    this.ctx.strokeRect(x, y, width, height);
    this.ctx.globalAlpha = fillAlpha;
    this.ctx.fillRect(x, y, width, height);
    this.ctx.globalAlpha = 1.0;

    if (num !== undefined) {
      this.ctx.fillStyle = 'red';
      // this.ctx.fillText('' + num, x + width / 2, y + height / 2);
      this.ctx.fillText('' + num, x + 5, y + height - 5);
      this.ctx.fillStyle = 'yellow';
    }
  }

  clear_canvas(): void {
    this.ctx.clearRect(
      0,
      0,
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );
  }

  public toAbsolute(rect: RectangleP): Array<number> {
    const xAbs = rect.x * window.innerWidth;
    const yAbs = rect.y * window.innerHeight;

    const widthAbs = rect.width * window.innerWidth;
    const heightAbs = rect.height * window.innerHeight;

    return [xAbs, yAbs, widthAbs, heightAbs];
  }

  navigate(path: string): boolean {
    this.router.navigateByUrl(path);

    return false;
  }

  pass(event: Event): void {
    event.preventDefault();
  }
}
