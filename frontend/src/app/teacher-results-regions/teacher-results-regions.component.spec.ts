import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherResultsRegionsComponent } from './teacher-results-regions.component';

describe('TeacherResultsRegionsComponent', () => {
  let component: TeacherResultsRegionsComponent;
  let fixture: ComponentFixture<TeacherResultsRegionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherResultsRegionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherResultsRegionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
