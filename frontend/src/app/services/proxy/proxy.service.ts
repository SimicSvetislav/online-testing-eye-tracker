import { Observable } from 'rxjs';
import { PROXY } from './../../globals';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProxyService {

  constructor(private http: HttpClient) { }

  start(questionId: number): Observable<any> {
    return this.http.get(PROXY + 'start/' + questionId, {responseType: 'text'});
  }

  question(questionId: number): Observable<any> {
    return this.http.get(PROXY + 'question/' + questionId, {responseType: 'text'});
  }

  finish(): Observable<any> {
    return this.http.get(PROXY + 'finish');
  }

  stop(): Observable<any> {
    return this.http.get(PROXY + 'stop', { responseType: 'text' });
  }

}
