import { JwtResponse } from './../../types/JwtResponse';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpRequest, HttpClient } from '@angular/common/http';
import { AuthLoginInfo } from 'src/app/types/AuthLoginInfo';
import { Observable, BehaviorSubject } from 'rxjs';
import { API } from './../../globals';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  cachedRequests: Array<HttpRequest<any>> = [];
  refresh = false;

  constructor(private router: Router, private http: HttpClient) { }

  public setToken(token: JwtResponse): void {
    localStorage.setItem('token', JSON.stringify(token));
  }

  public getToken(): string {
    const storedData: JwtResponse = JSON.parse(localStorage.getItem('token'));
    return storedData != null ? storedData.token : null;
  }

  public getUserId(): number {
    const storedData: JwtResponse = JSON.parse(localStorage.getItem('token'));
    return storedData != null ? storedData.id : null;
  }

  public getRole(): string {
    const storedData: JwtResponse = JSON.parse(localStorage.getItem('token'));
    return storedData.authorities[0].authority;
  }

  public deleteToken(): void {
    localStorage.removeItem('token');
    // localStorage.setItem('token', null);
  }

  public isAuthenticated(): boolean {
    return this.getToken() != null;
    // return tokenNotExpired(null, token);
  }

  public logOut(): void {
    this.deleteToken();
    this.router.navigate(['/login']);
  }

  public collectFailedRequest(request): void {
    this.cachedRequests.push(request);
  }

  public retryFailedRequests(): void {
    while (this.cachedRequests.length > 0) {
      const request: HttpRequest<any> = this.cachedRequests.pop();
      this.router.navigate([request.url]);
      break;
    }
    this.cachedRequests = [];
  }

  public attemptLogIn(info: AuthLoginInfo): Observable<any> {
    return this.http.post(API + 'auth/signin', info);
  }

  public checkLoggedIn(r: string): boolean {
    if (!this.isAuthenticated()) {
      this.router.navigate(['/login']);
    }

    const role = this.getRole();

    if (role !== r) {
      this.router.navigate(['/login']);
    }

    return true;
  }

  public setRefresh(value: boolean): void {
    this.refresh = value;
  }

  public getRefresh(): boolean {
    return this.refresh;
  }
}
