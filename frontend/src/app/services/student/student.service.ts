import { DataRecord } from './../../types/DataRecord';
import { HttpClient } from '@angular/common/http';
import { API_STUDENT } from './../../globals';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { TestingInfo } from 'src/app/types/TestingInfo';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  constructor(private http: HttpClient) { }

  getStudentProfile(userId: number): Observable<any> {
    return this.http.get(API_STUDENT + 'profile/' + userId);
  }

  getTestsNames(): Observable<any> {
    return this.http.get(API_STUDENT + 'tests/names');
  }

  getTest(testId: number): Observable<any>{
    return this.http.get(API_STUDENT + 'test/details/' + testId);
  }

  submitTest(testingInfo: TestingInfo): Observable<any> {
    return this.http.post(API_STUDENT + 'test/submit', testingInfo);
  }

  results(studentId: number): Observable<any> {
    return this.http.get(API_STUDENT + 'results/' + studentId);
  }

  submitEyeTrackerRecords(records: Array<DataRecord>, studentId: number): Observable<any> {
    return this.http.post(API_STUDENT + 'test/records/' + studentId, records, {responseType: 'text'});
  }

}
