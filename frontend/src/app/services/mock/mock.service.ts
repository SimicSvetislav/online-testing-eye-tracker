import { DataRecord } from './../../types/DataRecord';
import { Injectable } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class MockService {
  constructor() {}

  generateEyeTrackerData(): Array<DataRecord> {

    const records = new Array<DataRecord>();
    const startPoint = new Date();

    for (let i = 0; i < 216000; ++i) {

      records.push(this.generateRecord(startPoint, i));

    }

    return records;

  }

  generateRecord(startPoint: Date, i: number): DataRecord {
    const dr = new DataRecord();

    // dr.mediaId = 0;
    // dr.mediaName = 'NewMedia';

    dr.cnt = i;
    dr.time = new Date().getTime() - startPoint.getTime();
    dr.timetick = new Date().getTime();

    dr.fpogx = Math.random();
    dr.fpogy = Math.random();
    dr.fpogs = Math.random();
    dr.fpogd = Math.random();
    dr.fpogId = i;
    dr.fpogv = 1;

    dr.bpogx = Math.random();
    dr.bpogy = Math.random();
    dr.bpogv = 1;

    dr.cx = Math.random();
    dr.cy = Math.random();
    dr.cs = 0;

    // dr.user = '';

    dr.lpcx = Math.random();
    dr.lpcy = Math.random();
    dr.lpd = Math.random();
    dr.lps = Math.random();
    dr.lpv = Math.random();

    dr.rpcx = Math.random();
    dr.rpcy = Math.random();
    dr.rpd = Math.random();
    dr.rps = Math.random();
    dr.rpv = Math.random();

    return dr;
  }

}
