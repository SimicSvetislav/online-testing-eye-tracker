import { TestDTO } from './../../types/dtos/TestDTO';
import { API_TEACHER, API } from './../../globals';
import { HttpClient } from '@angular/common/http';
import { TeacherProfileDTO } from './../../types/dtos/TeacherProfileDTO';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http: HttpClient) { }

  getTeacherProfile(userId: number): Observable<any>{
    return this.http.get(API_TEACHER + 'profile/' + userId);
  }

  getTeacherTestNames(userId: number): Observable<any>{
    return this.http.get(API_TEACHER + userId + '/test/names');
  }

  getTeacherTestDetails(testId: number): Observable<any>{
    return this.http.get(API_TEACHER + 'test/details/' + testId);
  }

  getTeacherTestDetailsNew(testId: number): Observable<any>{
    return this.http.get(API_TEACHER + 'test/details/new/' + testId);
  }

  getQuestionTypes(): Observable<any>{
    return this.http.get(API_TEACHER + 'types');
  }

  postTest(test: TestDTO, teacherId: number): Observable<any> {
    return this.http.post(API_TEACHER + 'test/' + teacherId, test, {responseType: 'text'});
  }

  results(userId: number): Observable<any> {
    return this.http.get(API_TEACHER + 'results/' + userId);
  }

  getResultAnswer(testingInfoId: number): Observable<any> {
    return this.http.get(API_TEACHER + 'results/answers/' + testingInfoId);
  }

  getTeacherTestDetailsSorted(testId: number): Observable<any> {
    return this.http.get(API_TEACHER + 'test/details/sorted/' + testId);
  }

  getCSVFile(testingInfoId: number): Observable<any> {
    return this.http.get(API_TEACHER + 'csv2/' + testingInfoId, {responseType: 'text'});
  }

  postNewTest(test: TestDTO, teacherId: number): Observable<any> {
    return this.http.post(API_TEACHER + 'test/new/' + teacherId, test, {responseType: 'text'});
  }

  norm(): Observable<any> {
    return this.http.get(API_TEACHER + 'norm');
  }

}
