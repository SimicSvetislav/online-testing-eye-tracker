import { QuestionNewDTO } from './../types/dtos/QuestionNewDTO';
import { QuestionRegionSequenceDTO } from './../types/dtos/QuestionRegionSequenceDTO';
import { RectangleP } from './../types/RectangleP';
import { QuestionDTO } from './../types/dtos/QuestionDTO';
import { TestDTO } from './../types/dtos/TestDTO';
import { ReportAnswerDTO } from './../types/dtos/ReportAnswerDTO';
import { TeacherService } from './../services/teacher/teacher.service';
import { ReportDTO } from './../types/dtos/ReportDTO';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-teacher-results',
  templateUrl: './teacher-results.component.html',
  styleUrls: ['./teacher-results.component.scss'],
})
export class TeacherResultsComponent implements OnInit {
  logged = false;
  userId: number;

  results: Array<ReportDTO>;

  report = new ReportDTO();
  view = 'list';

  collapsed = new Array<boolean>();
  qCollapsed = new Array<boolean>();

  answerReport = new ReportAnswerDTO();

  questionNum: number;

  test = new TestDTO();
  currentQuestion = new QuestionNewDTO();
  // regionID -> index
  regionMapping = new Map<number, number>();

  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;

  private ctx: CanvasRenderingContext2D;

  regionShowBools: Array<boolean>;
  regionShowTextArea: string;

  regionsSequenceInfo: Array<string>;

  color = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private teacherService: TeacherService
  ) {}

  ngOnInit(): void {
    this.logged = this.authService.checkLoggedIn('ROLE_TEACHER');
    this.userId = this.authService.getUserId();

    /*this.teacherService.norm().subscribe((arg) => {
      console.log(arg);
    });*/

    this.teacherService.results(this.userId).subscribe((data) => {
      this.results = data;
      this.results.forEach((_) => {
        this.collapsed.push(true);
      });
    });

    this.initCanvas();
  }

  initCanvas(): void {
    this.canvas.nativeElement.height = window.innerHeight;
    this.canvas.nativeElement.width = window.innerWidth;

    this.ctx = this.canvas.nativeElement.getContext('2d');

    this.ctx.strokeStyle = 'blue';
    this.ctx.fillStyle = 'yellow';
    this.ctx.lineWidth = 1;
    this.ctx.font = '18px Arial';
  }

  private draw_rects(fillAlpha = 0.05): void {
    this.currentQuestion.regions.forEach((rect) => {
      this.draw_rect(rect, fillAlpha, this.regionMapping.get(rect.id));
    });
  }

  private draw_rect(rect: RectangleP, fillAlpha: number, num?: number): void {
    const [x, y, width, height] = this.toAbsolute(rect);

    this.ctx.globalAlpha = 1.0;
    this.ctx.strokeRect(x, y, width, height);
    this.ctx.globalAlpha = fillAlpha;
    this.ctx.fillRect(x, y, width, height);
    this.ctx.globalAlpha = 1.0;

    if (num !== undefined) {
      this.ctx.fillStyle = 'red';
      this.ctx.fillText('' + num, x + width / 2, y + height / 2);
      this.ctx.fillStyle = 'yellow';
    }
  }

  clear_canvas(): void {
    this.ctx.clearRect(
      0,
      0,
      this.canvas.nativeElement.width,
      this.canvas.nativeElement.height
    );
  }

  public toAbsolute(rect: RectangleP): Array<number> {
    const xAbs = rect.x * window.innerWidth;
    const yAbs = rect.y * window.innerHeight;

    const widthAbs = rect.width * window.innerWidth;
    const heightAbs = rect.height * window.innerHeight;

    return [xAbs, yAbs, widthAbs, heightAbs];
  }

  navigate(path: string): boolean {
    this.router.navigateByUrl(path);

    return false;
  }

  logout(): void {
    this.authService.logOut();
    this.router.navigate(['/login']);
  }

  pass(event: Event): void {
    event.preventDefault();
  }

  viewAnswers(r: ReportDTO): void {
    this.view = 'answers';
    this.report = r;

    this.teacherService.getResultAnswer(r.id).subscribe((res) => {
      this.answerReport = res as ReportAnswerDTO;
      this.qCollapsed = new Array<boolean>();
      this.answerReport.qreports.forEach((_) => {
        this.qCollapsed.push(true);
      });
    });
  }

  viewRegions(r: ReportDTO): void {
    sessionStorage.setItem('report', JSON.stringify(r));
    this.navigate('/teacher/results/regions');
    /*this.view = 'regions';
    this.questionNum = 1;
    this.report = r;

    this.regionShowBools = new Array<boolean>();
    this.regionShowTextArea = '';

    this.initCanvas();

    // this.teacherService.getTeacherTestDetailsSorted(r.testId).subscribe((data) => {
    this.teacherService.getTeacherTestDetailsNew(r.testId).subscribe((data) => {
      this.test = data;
      this.currentQuestion = this.test.questionElements[this.questionNum - 1];

      this.currentQuestion.regions.forEach((region, index) => {
        this.regionMapping.set(region.id, index + 1);
      });

      this.teacherService.getResultAnswer(this.report.id).subscribe((res) => {
        this.answerReport = res as ReportAnswerDTO;
        this.prepareAnswers();
        this.fillRegionsInfoString();
      });
      this.draw_rects();
    });*/
  }

  prepareAnswers(): void {
    this.regionShowBools = new Array<boolean>();
    this.regionShowTextArea = '';
    if (this.currentQuestion.type.id === 1) {
      this.currentQuestion.answerElements.forEach((el) => {
        const checked = this.answerReport.qreports[
          this.questionNum - 1
        ].answered.includes(el.text);
        this.regionShowBools.push(checked);
      });
    } else if (this.currentQuestion.type.id === 2) {
      this.regionShowTextArea = this.answerReport.qreports[
        this.questionNum - 1
      ].answered[0];
    }
  }

  back(): void {
    this.view = 'list';
    this.report = new ReportDTO();
  }

  previousQuestion(): void {
    this.questionNum--;

    this.currentQuestion = this.test.questionElements[this.questionNum - 1];

    this.currentQuestion.regions.forEach((region, index) => {
      this.regionMapping.set(region.id, index + 1);
    });

    this.fillRegionsInfoString();

    this.teacherService.getResultAnswer(this.report.id).subscribe((res) => {
      this.answerReport = res as ReportAnswerDTO;
      this.prepareAnswers();
    });

    this.clear_canvas();
    this.draw_rects();
  }

  nextQuestion(): void {
    this.questionNum++;

    this.currentQuestion = this.test.questionElements[this.questionNum - 1];

    this.currentQuestion.regions.forEach((region, index) => {
      this.regionMapping.set(region.id, index + 1);
    });

    this.fillRegionsInfoString();

    this.teacherService.getResultAnswer(this.report.id).subscribe((res) => {
      this.answerReport = res as ReportAnswerDTO;
      this.prepareAnswers();
    });

    this.clear_canvas();
    this.draw_rects();
  }

  fillRegionsInfoString(): void {
    this.regionsSequenceInfo = new Array<string>();
    this.answerReport.qrs.forEach((s, index) => {
      if (s.questionId === this.currentQuestion.id) {
        let str = s.regionSequence.filter((seq) => seq.length > 0).toString();
        const splitted = str.split(',');
        const mappedIds = new Array<number>();
        splitted.forEach((spl) => {
          mappedIds.push(this.regionMapping.get(+spl));
        });
        str = mappedIds.join(' -> ');
        this.regionsSequenceInfo.push(str);
      }
    });
  }

  getCSVFile(r: ReportDTO): void {
    this.teacherService.getCSVFile(r.id).subscribe((data) => {
      const blob = new Blob([data], { type: 'text/csv' });
      const filename =
        r.studentIndex +
        '_' +
        r.attempt +
        '_' +
        r.testName;
      saveAs(blob, filename + '.csv');
      // alert(data);
    });
  }
}
