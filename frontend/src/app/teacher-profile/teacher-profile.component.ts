import { TeacherService } from './../services/teacher/teacher.service';
import { TeacherProfileDTO } from './../types/dtos/TeacherProfileDTO';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-teacher-profile',
  templateUrl: './teacher-profile.component.html',
  styleUrls: ['./teacher-profile.component.scss']
})
export class TeacherProfileComponent implements OnInit {

  logged = false;
  userId: number;

  teacher = new TeacherProfileDTO();

  constructor(private authService: AuthService, private router: Router,
              private teacherService: TeacherService) { }

  ngOnInit(): void {

    this.logged = this.authService.checkLoggedIn('ROLE_TEACHER');
    this.userId = this.authService.getUserId();

    this.teacherService.getTeacherProfile(this.userId).subscribe(arg => {
      this.teacher = arg;
    }, error => {
      console.log(error);
    });

    // alert(this.userId);

  }

  navigate(path: string): boolean {

    this.router.navigateByUrl(path);

    return false;

  }

  logout(): void {
    this.authService.logOut();
    this.router.navigate(['/login']);
  }

  pass(event: Event): void {
    event.preventDefault();
  }

}
