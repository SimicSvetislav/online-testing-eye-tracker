import { DataRecord } from './DataRecord';
import { TestAnswer } from './TestAnswer';
export class TestingInfo {
    testId: number;
    studentId: number;

    testAnswers: Array<TestAnswer>;
}
