import { Point } from './Point';
/**
 * @deprecated
 */
export class Rectangle {
    topLeft: Point;
    bottomRight: Point;
    width: number;
    height: number;

    constructor(p1: Point, p2: Point) {

        this.topLeft = p1;
        this.bottomRight = p2;

        this.width = p2.x - p1.x;
        this.height = p2.y - p1.y;
    }

}
