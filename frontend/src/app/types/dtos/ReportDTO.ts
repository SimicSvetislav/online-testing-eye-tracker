import { ReportAnswerDTO } from './ReportAnswerDTO';

export class ReportDTO {
    id: number;
    testId: number;
    testName: string;
    attempt: number;
    correctAnswers: number;
    essayQuestions: number;
    questionsNum: number;

    studentIndex: string;

    // testAnswers: Array<ReportAnswerDTO>;
}
