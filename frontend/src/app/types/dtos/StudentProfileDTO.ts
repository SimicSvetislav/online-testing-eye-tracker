export class StudentProfileDTO {
    id: number;
    fullName: string;
    displayName: string;
    email: string;
    indexNum: number;
    solvedTests: number;
    solvedTestsUnique: number;
}
