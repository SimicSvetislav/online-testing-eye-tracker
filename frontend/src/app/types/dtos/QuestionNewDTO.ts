import { ButtonElement } from './../elements/ButtonElement';
import { TextAreaElement } from '../elements/TextAreaElement';
import { AnswerElement } from '../elements/AnswerElement';
import { ImageElement } from '../elements/ImageElement';
import { TextElement } from '../elements/TextElement';
import { TitleElement } from '../elements/TitleElement';
import { RectangleP } from '../RectangleP';
import { AnswerDTO } from './AnswerDTO';
import { QuestionType } from '../QuestionType';

export class QuestionNewDTO {
    id: number;

    titleElements: Array<TitleElement>;
    textElements: Array<TextElement>;
    imageElements: Array<ImageElement>;
    answerElements: Array<AnswerElement>;
    textAreaElements: Array<TextAreaElement>;

    btnNextElement = new ButtonElement();
    btnPreviousElement = new ButtonElement();

    type = new QuestionType();

    regions: Array<RectangleP>;
}
