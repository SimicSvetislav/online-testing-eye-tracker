import { RectangleP } from './../RectangleP';
import { QuestionType } from './../QuestionType';
import { RegionDTO } from './RegionDTO';
import { AnswerDTO } from './AnswerDTO';

export class QuestionDTO {
    id: number;
    title: string;
    text: string;

    type = new QuestionType();

    answers: Array<AnswerDTO> = new Array<AnswerDTO>();
    regions: Array<RectangleP>;
}
