import { QuestionRegionSequenceDTO } from './QuestionRegionSequenceDTO';
import { QuestionReportDTO } from './QuestionReportDTO';

export class ReportAnswerDTO {

    id: number;

    qreports = new Array<QuestionReportDTO>();

    qrs = new Array<QuestionRegionSequenceDTO>();

}
