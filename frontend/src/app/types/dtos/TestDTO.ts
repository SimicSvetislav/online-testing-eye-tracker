import { QuestionNewDTO } from './QuestionNewDTO';
import { QuestionDTO } from './QuestionDTO';

export class TestDTO {
    id: number;
    name: string;
    duration: number;

    questions = new Array<QuestionDTO>();
    questionElements = new Array<QuestionNewDTO>();
}
