export class TestSimpleDTO {
    id: number;
    name: string;
    duration: number;

    questionsNum: number;
}
