export class QuestionRegionSequenceDTO {
    questionId: number;
    regionSequence: Array<Array<number>>;
}
