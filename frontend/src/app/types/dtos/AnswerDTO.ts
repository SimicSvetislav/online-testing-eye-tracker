export class AnswerDTO {
    id: number;
    correct: boolean;
    text: string;
}
