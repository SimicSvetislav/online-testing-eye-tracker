export class QuestionReportDTO {
    text: string;
    correct: boolean;
    qtypeId: number;

    answered: Array<string>;
    correctAnswer: Array<string>;
}
