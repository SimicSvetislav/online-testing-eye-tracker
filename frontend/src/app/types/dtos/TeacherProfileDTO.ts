export class TeacherProfileDTO {
    id: number;
    fullName: string;
    displayName: string;
    email: string;
    phoneNumber: string;
    testsNum: number;
}
