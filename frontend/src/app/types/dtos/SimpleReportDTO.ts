export class SimpleReportDTO {
    id: number;
    testName: string;
    questions: number;
    correctAnswers: number;
    essayQuestions: number;
}
