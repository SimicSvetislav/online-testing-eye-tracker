export class RectangleP {
    id: number;
    x: number;
    y: number;
    width: number;
    height: number;

    constructor(x?: number, y?: number, width?: number, height?: number) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public toAbsolute(): Array<number> {

        const xAbs = this.x * window.innerWidth;
        const yAbs = this.y * window.innerHeight;

        const widthAbs = this.width * window.innerWidth;
        const heightAbs = this.height * window.innerHeight;

        return [xAbs, yAbs, widthAbs, heightAbs];
    }

}
