import { TestAnswer } from './TestAnswer';

export class EssayTestAnswer extends TestAnswer {
    answer: string;
}
