export class IdAnswerPair {
    answerId: number;
    givenAnswer: boolean;
}
