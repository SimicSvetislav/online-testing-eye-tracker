import { BaseElement } from './BaseElement';

export class ImageElement extends BaseElement {
    imgURL: ArrayBuffer | string;
}
