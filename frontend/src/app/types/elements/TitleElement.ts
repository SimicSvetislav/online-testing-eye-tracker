import { BaseElement } from './BaseElement';

export class TitleElement extends BaseElement {
    titleText: string;
}
