import { BaseElement } from './BaseElement';

export class TextAreaElement extends BaseElement {
    redova: number;
    kolona: number;
    placeholder: string;
}
