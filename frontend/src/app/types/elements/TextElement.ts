import { TextLine } from './TextLine';
import { BaseElement } from './BaseElement';

export class TextElement extends BaseElement {
    lines: Array<string>;
    textLines = new  Array<TextLine>();
}
