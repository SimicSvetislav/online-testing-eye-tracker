import { BaseElement } from './BaseElement';

export class AnswerElement extends BaseElement {
    text: string;
    correct = false;
}
