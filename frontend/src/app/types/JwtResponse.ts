import { Authority } from './Authority';

export class JwtResponse {
    token: string;
    tokenType: string;
    displayName: string;
    authorities: Authority[];
    id: number;
}
