import { IdAnswerPair } from './IdAnswerPair';
import { TestAnswer } from './TestAnswer';

export class MCTestAnswer extends TestAnswer {
    answer: Array<IdAnswerPair>;
}
