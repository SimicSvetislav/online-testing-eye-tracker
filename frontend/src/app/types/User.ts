export class User {
    id: number;
    fullName: string;
    displayName: string;
    password: string;
    email: string;

    phoneNumber: string;

    indexNum: string;
    
}