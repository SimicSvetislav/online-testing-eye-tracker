export class DataRecord {
    // mediaId: number;
    // mediaName: string;

    cnt: number;
    time: number;
    timetick: number;

    fpogx: number;
    fpogy: number;
    fpogs: number;
    fpogd: number;
    fpogId: number;
    fpogv: number;

    bpogx: number;
    bpogy: number;
    bpogv: number;

    lpcx: number;
    lpcy: number;
    lpd: number;
    lps: number;
    lpv: number;

    rpcx: number;
    rpcy: number;
    rpd: number;
    rps: number;
    rpv: number;

    cx: number;
    cy: number;
    cs: number;

    questionId: number;

    // user: string;

    // question: number;

    /*bkId: number;
    bkDur: number;
    bkpMin: number;

    eegFr: number;
    eegEn: number;
    eegMed: number;
    eegEx: number;*/

}
