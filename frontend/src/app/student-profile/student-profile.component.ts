import { StudentService } from './../services/student/student.service';
import { StudentProfileDTO } from './../types/dtos/StudentProfileDTO';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-profile',
  templateUrl: './student-profile.component.html',
  styleUrls: ['./student-profile.component.scss']
})
export class StudentProfileComponent implements OnInit {

  logged = false;
  userId: number;

  student = new StudentProfileDTO();

  constructor(private authService: AuthService, private router: Router,
              private studentService: StudentService) { }

  ngOnInit(): void {

    this.logged = this.authService.checkLoggedIn('ROLE_STUDENT');
    this.userId = this.authService.getUserId();

    this.studentService.getStudentProfile(this.userId).subscribe(student => {
      this.student = student;
    }, error => {
      console.log(error);
    });

  }

  navigate(path: string): boolean {

    this.router.navigateByUrl(path);

    return false;

  }

  logout(): void {
    this.authService.logOut();
    this.router.navigate(['/login']);
  }

  pass(event: Event): void {
    event.preventDefault();
  }

}
