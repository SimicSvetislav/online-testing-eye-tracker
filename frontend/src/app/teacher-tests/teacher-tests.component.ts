import { TestDTO } from './../types/dtos/TestDTO';
import { TeacherService } from './../services/teacher/teacher.service';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-teacher-tests',
  templateUrl: './teacher-tests.component.html',
  styleUrls: ['./teacher-tests.component.scss']
})
export class TeacherTestsComponent implements OnInit {

  logged = false;
  userId: number;

  createdTests: Array<TestDTO>;

  constructor(private authService: AuthService, private router: Router,
              private teacherService: TeacherService) { }

  ngOnInit(): void {

    this.logged = this.authService.checkLoggedIn('ROLE_TEACHER');
    this.userId = this.authService.getUserId();

    this.teacherService.getTeacherTestNames(this.userId).subscribe(data => {
      this.createdTests = data;
      // this.createdTests.push({name: 'Prvi', id: 1, duration: 10});
      // this.createdTests.push({name: 'Drugi', id: 1, duration: 10});
    });

  }

  navigate(path: string): boolean {

    this.router.navigateByUrl(path);

    return false;

  }

  logout(): void {
    this.authService.logOut();
    this.router.navigate(['/login']);
  }

  login(): void {
    this.router.navigate(['/login']);
  }

  pass(event: Event): void {
    event.preventDefault();
  }

}
