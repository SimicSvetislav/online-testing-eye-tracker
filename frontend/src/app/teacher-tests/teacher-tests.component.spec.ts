import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherTestsComponent } from './teacher-tests.component';

describe('TeacherTestsComponent', () => {
  let component: TeacherTestsComponent;
  let fixture: ComponentFixture<TeacherTestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherTestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherTestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
