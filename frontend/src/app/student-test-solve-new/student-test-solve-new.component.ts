import { SimpleReportDTO } from './../types/dtos/SimpleReportDTO';
import { DataRecord } from './../types/DataRecord';
import { TestingInfo } from './../types/TestingInfo';
import { TestAnswer } from './../types/TestAnswer';
import { EssayTestAnswer } from './../types/EssayTestAnswer';
import { MCTestAnswer } from './../types/MCTestAnswer';
import { IdAnswerPair } from './../types/IdAnswerPair';
import { ProxyService } from './../services/proxy/proxy.service';
import { StudentService } from './../services/student/student.service';
import { TestDTO } from './../types/dtos/TestDTO';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { QuestionNewDTO } from './../types/dtos/QuestionNewDTO';
import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-student-test-solve-new',
  templateUrl: './student-test-solve-new.component.html',
  styleUrls: ['./student-test-solve-new.component.scss'],
})
export class StudentTestSolveNewComponent implements OnInit {
  logged = false;
  userId: number;

  testId: number;

  test = new TestDTO();
  currentQuestion: QuestionNewDTO;
  questionCounter: number;
  testAnswers = new Array<TestAnswer>();
  essayQuestionsExist = false;

  startPoint: Date;

  wait: boolean;
  phaseSolving: boolean;

  records = new Array<DataRecord>();

  simpleReport: SimpleReportDTO;

  color = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private studentService: StudentService,
    private proxyService: ProxyService
  ) {}

  ngOnInit(): void {
    this.logged = this.authService.checkLoggedIn('ROLE_STUDENT');
    this.userId = this.authService.getUserId();

    this.testId = this.route.snapshot.params.id;

    this.studentService.getTest(this.testId).subscribe((data) => {
      this.test = data;
      this.currentQuestion = this.test.questionElements[0];
      this.questionCounter = 1;

      this.test.questionElements.forEach((q) => {
        if (q.type.id === 1) {
          const idAnswerPairs = new Array<IdAnswerPair>();
          for (const answ of q.answerElements) {
            const idAnswerPair = new IdAnswerPair();
            idAnswerPair.givenAnswer = false;
            idAnswerPair.answerId = answ.id;
            idAnswerPairs.push(idAnswerPair);
          }

          const mcTestAnswer = new MCTestAnswer();
          mcTestAnswer.questionId = q.id;
          mcTestAnswer.answer = idAnswerPairs;

          this.testAnswers.push(mcTestAnswer);
        } else if (q.type.id === 2) {
          this.essayQuestionsExist = true;
          const essayTestAnswer = new EssayTestAnswer();
          essayTestAnswer.questionId = q.id;
          essayTestAnswer.answer = '';

          this.testAnswers.push(essayTestAnswer);
        }
      });

      this.proxyService.start(this.currentQuestion.id).subscribe((res) => {
        console.log(res);
      });
    });

    this.startPoint = new Date();
    this.phaseSolving = true;
  }

  navigate(path: string): boolean {
    this.proxyService.stop().subscribe((res) => {
      console.log(res);
    });
    this.router.navigateByUrl(path);

    return false;
  }

  nextQuestion(): void {
    this.currentQuestion = this.test.questionElements[this.questionCounter++];
    this.updateQuestionProxy();
  }

  previousQuestion(): void {
    this.questionCounter--;
    this.currentQuestion = this.test.questionElements[this.questionCounter - 1];
    this.updateQuestionProxy();
  }

  updateQuestionProxy(): void {
    this.proxyService.question(this.currentQuestion.id).subscribe((res) => {
      console.log(res);
    });
  }

  finishTest(): void {
    this.phaseSolving = false;
    this.wait = true;

    const testingInfo = new TestingInfo();

    testingInfo.studentId = this.userId;
    testingInfo.testId = this.testId;
    testingInfo.testAnswers = this.testAnswers;

    // this.records = this.mockService.generateEyeTrackerData();

    this.proxyService.finish().subscribe((data) => {
      this.records = data as Array<DataRecord>;
      this.studentService.submitTest(testingInfo).subscribe((report) => {
        this.simpleReport = report;
        this.wait = false;
        this.studentService
          .submitEyeTrackerRecords(this.records, this.simpleReport.id)
          .subscribe((response) => {
            console.log(response);
          });
      });
    });
  }

  close(): void {
    this.navigate('/student/tests');
  }

  @HostListener('window:beforeunload', ['$event'])
  handleBeforeUnload($event: any): void {
    if (this.phaseSolving) {
      $event.preventDefault();
      // alert('Current answers will be recorder');
      this.finishTest();
    }
  }
}
