import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentTestSolveNewComponent } from './student-test-solve-new.component';

describe('StudentTestSolveNewComponent', () => {
  let component: StudentTestSolveNewComponent;
  let fixture: ComponentFixture<StudentTestSolveNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentTestSolveNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentTestSolveNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
