import { JwtResponse } from './../types/JwtResponse';
import { User } from './../types/User';
import { AuthLoginInfo } from './../types/AuthLoginInfo';
import { AuthService } from './../services/auth/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  user: User = new User();
  constructor(private router: Router, private authService: AuthService,
              private autService: AuthService, private toastr: ToastrService) { }
  str = '';

  private loginInfo: AuthLoginInfo;
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  ngOnInit(): void {
    if (this.authService.isAuthenticated()) {

      const role = this.autService.getRole();

      if (role === 'ROLE_TEACHER') {
        this.router.navigate(['/teacher/profile']);
      } else if (role === 'ROLE_STUDENT') {
        this.router.navigate(['/student/profile']);
      }
    }

    // this.toastr.success('Hello world!', 'Toastr fun!');
  }

  home(): void {
    this.router.navigate(['/profile']);
  }

  login(): void {
    // alert("Usao!")
    this.loginInfo = new AuthLoginInfo(this.user.displayName, this.user.password);
    this.authService.attemptLogIn(this.loginInfo).subscribe(data => {

      if (data.token === undefined) {
        alert('Nesto nije u redu!');
      } else {

        this.authService.setToken(data as JwtResponse);
        this.authService.isUserLoggedIn.next(true);

        this.isLoginFailed = false;
        this.isLoggedIn = true;

        const role = data.authorities[0].authority;

        if (role === 'ROLE_TEACHER') {
          this.router.navigate(['/teacher/profile']);
        } else if (role === 'ROLE_STUDENT') {
          this.router.navigate(['/student/profile']);
        }

      }

    }, error => {
      console.log(error);
    });
  }

}
