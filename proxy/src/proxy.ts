import { DataRecord } from "./DataRecord";
import { Socket, AddressInfo } from "net";

import express from "express";
import bodyParser from "body-parser";
import {
  ENABLE_CNT,
  ENABLE_TIME,
  ENABLE_TIME_TICK,
  ENABLE_POG_BEST,
  ENABLE_POG,
  ENABLE_PUPIL_LEFT,
  ENABLE_PUPIL_RIGHT,
  ENABLE_CURSOR,
  ENABLE_SENDIND_DATA,
  DISABLE_SENDIND_DATA,
} from "./constants";
import { strict } from "assert";

const client = new Socket();
client.connect({
  port: 4242,
});

let records: DataRecord[];
// let recordsStr: string[];
let questionId: number;

client.on("connect", () => {
  // tslint:disable-next-line: no-console
  console.log("Proxy: connection established with eye tracker");

  const address = client.address() as AddressInfo;
  const port = address.port;
  const family = address.family;
  const ipaddr = address.address;
  // tslint:disable-next-line: no-console
  console.log("Client is listening at port : " + port);
  // tslint:disable-next-line: no-console
  console.log("Client ip : " + ipaddr);
  // tslint:disable-next-line: no-console
  console.log("Client is IP4/IP6 : " + family);

  // tslint:disable-next-line: no-console
  console.log("Sending configuration data");

  // Send XML commands
  /*client.write(ENABLE_CNT);
  client.write(ENABLE_TIME);
  client.write(ENABLE_TIME_TICK);

  client.write(ENABLE_POG);

  client.write(ENABLE_POG_BEST);

  client.write(ENABLE_PUPIL_LEFT);

  client.write(ENABLE_PUPIL_RIGHT);

  client.write(ENABLE_CURSOR);*/

  sendConfigData();

  async function sendConfigData() {
    client.write(ENABLE_CNT);
    await sleep(10);
    client.write(ENABLE_TIME);
    await sleep(10);
    client.write(ENABLE_TIME_TICK);

    await sleep(10);
    client.write(ENABLE_POG);

    await sleep(10);
    client.write(ENABLE_POG_BEST);

    await sleep(10);
    client.write(ENABLE_PUPIL_LEFT);

    await sleep(10);
    client.write(ENABLE_PUPIL_RIGHT);

    await sleep(10);
    client.write(ENABLE_CURSOR);
  }
});

client.setEncoding("utf8");

client.on("data", (data) => {
  const strData = data.toString();
  try {
    // Simple string data
    // const record = JSON.parse(str) as DataRecord;
    // record.questionId = questionId;

    const tag = getTag(strData);
    // tslint:disable-next-line: no-console
    // console.log("Tag : " + tag);
    if (tag === "REC") {
      const record = parseData(strData);
      record.questionId = questionId;
      // tslint:disable-next-line: no-console
      // console.log("Data from server : " + record.questionId);
      records.push(record);
    }

    // tslint:disable-next-line: no-console
    // console.log("Data from server : " + record.questionId);

    // recordsStr.push(str);
  } catch (error) {
    // tslint:disable-next-line: no-console
    console.log("[ERROR] Data from server : " + data);
  }
});

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

/*setTimeout(() => {
    client.end("Bye bye server");
}, 5000);*/

// API

const app = express();
const apiPort = 7070;

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");

  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,Authorization"
  );

  // res.setHeader('Access-Control-Allow-Credentials', 'true');
  // res.header('Access-Control-Expose-Headers', 'Authorization');

  next();
});

app.get("/", (req, res) => {
  res.send("Proxy server is running");
});

app.get("/api/start/:qId", (req, res) => {
  records = new Array<DataRecord>();
  // recordsStr = new Array<string>();
  questionId = +req.params.qId;
  // tslint:disable-next-line: no-console
  console.log("Start collecting data");
  // client.write("start");
  client.write(ENABLE_SENDIND_DATA);
  res.status(200).send("Data collection started");
});

app.get("/api/question/:qId", (req, res) => {
  questionId = +req.params.qId;
  // tslint:disable-next-line: no-console
  console.log("Updating question ID");
  res.status(200).send("Question ID updated");
});

app.get("/api/finish", (req, res) => {
  // tslint:disable-next-line: no-console
  console.log("Finishing collecting data");
  // client.write("stop");
  client.write(DISABLE_SENDIND_DATA);
  res.status(200).send(JSON.stringify(records));
  records = new Array<DataRecord>();
  // recordsStr = new Array<string>();
});

app.get("/api/stop", (req, res) => {
  // tslint:disable-next-line: no-console
  console.log("Stopping data collection");
  client.write("stop");
  res.status(200).send("Data collection stopped");
  records = new Array<DataRecord>();
  // recordsStr = new Array<string>();
});

app.listen(apiPort, () => {
  // tslint:disable-next-line:no-console
  console.log("Proxy server started at http://localhost:" + apiPort);
});

function getTag(xmlString: string) {
  const temp = xmlString.substring(xmlString.indexOf("<") + 1);

  const tag = temp.split(" ")[0];

  return tag;
}

function getAttribute(data: string, attr: string): string {
  const temp = data.substring(data.indexOf(`${attr}=`) + attr.length + 2);

  const attrVal = temp.split('"')[0];

  return attrVal;
}

function getAttributeReverse(data: string, attr: string): string {
  const temp = data.substring(data.lastIndexOf(`${attr}=`) + attr.length + 2);

  const attrVal = temp.split('"')[0];

  return attrVal;
}

function parseData(data: string): DataRecord {
  const retVal = new DataRecord();

  retVal.cnt = +getAttribute(data, "CNT");
  retVal.time = +getAttribute(data, "TIME");
  retVal.timetick = +getAttribute(data, "TIME_TICK");

  retVal.fpogx = +getAttribute(data, "FPOGX");
  retVal.fpogy = +getAttribute(data, "FPOGY");
  retVal.fpogs = +getAttribute(data, "FPOGS");
  retVal.fpogd = +getAttribute(data, "FPOGD");
  retVal.fpogId = +getAttribute(data, "FPOGID");
  retVal.fpogv = +getAttribute(data, "FPOGV");

  retVal.bpogx = +getAttribute(data, "BPOGX");
  retVal.bpogy = +getAttribute(data, "BPOGY");
  retVal.bpogv = +getAttribute(data, "BPOGV");

  retVal.lpcx = +getAttribute(data, "LPCX");
  retVal.lpcy = +getAttribute(data, "LPCY");
  retVal.lpd = +getAttribute(data, "LPD");
  retVal.lps = +getAttribute(data, "LPS");
  retVal.lpv = +getAttribute(data, "LPV");

  retVal.rpcx = +getAttribute(data, "RPCX");
  retVal.rpcy = +getAttribute(data, "RPCY");
  retVal.rpd = +getAttribute(data, "RPD");
  retVal.rps = +getAttribute(data, "RPS");
  retVal.rpv = +getAttribute(data, "RPV");

  retVal.cx = +getAttributeReverse(data, "CX");
  retVal.cy = +getAttributeReverse(data, "CY");
  retVal.cs = +getAttribute(data, "CS");

  return retVal;
}
