var net = require("net");
var server = net.createServer();

var streaming = false;

server.maxConnections = 10;
PORT = 4242;

server.on("close", function () {
  console.log("Server closed");
});

server.on("connection", function (socket) {
  socket.setEncoding("utf8");

  console.log("Client connected");

  socket.on("data", function (data) {
    var bread = socket.bytesRead;
    var bwrite = socket.bytesWritten;
    console.log("Bytes read : " + bread);
    console.log("Bytes written : " + bwrite);
    console.log("Received : " + data);

    // XML commands
    const command = data.toString();
    const tag = getTag(command);
    console.log(tag);

    if (tag === "SET") {
      const id = getId(command);
      console.log(id);

      const state = getState(command);
      console.log(state);

      if (id === "ENABLE_SEND_DATA") {
        if (state === "1") {
          streaming = true;
          stream();
        } else if (state === "0") {
          streaming = false;
        }
      }
    }

    // Simple string commands
    /*if (command === "start") {
      streaming = true;
      stream();
    } else if (command === "stop") {
      streaming = false;
    }*/

    // var is_kernel_buffer_full = socket.write("Data ::" + data);
    /*if (is_kernel_buffer_full) {
      console.log(
        "Data was flushed successfully from kernel buffer i.e written successfully!"
      );
    } else {
      socket.pause();
    }*/
  });

  socket.on("error", function (error) {
    console.log("Error : " + error);
  });

  socket.on("timeout", function () {
    console.log("Socket timed out !");
    socket.end("Timed out!");
  });

  socket.on("end", function (data) {
    console.log("Socket ended from other end!");
    console.log("End data : " + data);
  });

  socket.on("close", function (error) {
    var bread = socket.bytesRead;
    var bwrite = socket.bytesWritten;
    console.log("Bytes read : " + bread);
    console.log("Bytes written : " + bwrite);
    console.log("Socket closed!");
    if (error) {
      console.log("Socket was closed coz of transmission error");
    }

    streaming = false;
  });

  async function stream() {
    startPoint = new Date();
    i = 0;
    while (streaming) {
      // let num = Math.random();
      const data = generateRecord(startPoint, i);
      // const dataStr = JSON.stringify(data);
      dataStr = `<REC CNT="${data.cnt}" TIME="${data.time}" TIME_TICK="${data.timetick}" FPOGX="${data.fpogx}" FPOGY="${data.fpogy}" FPOGS="${data.fpogs}" FPOGD="${data.fpogd}" FPOGID="${data.fpogId}" FPOGV="${data.fpogv}" BPOGX="${data.bpogx}" BPOGY="${data.bpogy}" BPOGV="${data.bpogv}" LPCX="${data.lpcx}" LPCY="${data.lpcy}" LPD="${data.lpd}" LPS="${data.lps}" LPV="${data.lpv}" RPCX="${data.rpcx}" RPCY="${data.rpcy}" RPD="${data.rpd}" RPS="${data.rps}" RPV="${data.rpv}" CX="${data.cx}" CY="${data.cy}" CS="${data.cs}" /> \r\n`;
      
      console.log("Sending : " + dataStr);
      socket.write(dataStr);
      i++;
      await sleep(16);
    }
  }
});

server.on("error", function (error) {
  console.log("Error: " + error);
});

server.on("listening", function () {
  console.log("Server is listening on port " + PORT);
});

server.listen(PORT);

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function generateRecord(startPoint, i) {
  const dr = new Object();

  // dr.mediaId = 0;
  // dr.mediaName = 'NewMedia';

  dr.cnt = i;
  dr.time = new Date().getTime() - startPoint.getTime();
  dr.timetick = new Date().getTime();

  dr.fpogx = Math.random();
  dr.fpogy = Math.random();
  dr.fpogs = Math.random();
  dr.fpogd = Math.random();
  dr.fpogId = i;
  dr.fpogv = Math.random()<0.2 ? 0 : 1;

  dr.bpogx = Math.random();
  dr.bpogy = Math.random();
  dr.bpogv = Math.random()<0.2 ? 0 : 1;

  dr.cx = Math.random();
  dr.cy = Math.random();
  dr.cs = Math.random()<0.2 ? 1 : Math.random()<0.2 ? 2 : 0;

  // dr.user = '';

  dr.lpcx = Math.random();
  dr.lpcy = Math.random();
  dr.lpd = Math.random();
  dr.lps = Math.random();
  dr.lpv = Math.random()<0.2 ? 0 : 1;;

  dr.rpcx = Math.random();
  dr.rpcy = Math.random();
  dr.rpd = Math.random();
  dr.rps = Math.random();
  dr.rpv = Math.random()<0.2 ? 0 : 1;

  return dr;
}

function getTag(command) {
  let temp = command.substring(command.indexOf("<") + 1);

  let tag = temp.split(" ")[0];

  return tag;
}

function getId(command) {
  let temp = command.substring(command.indexOf("ID=") + 4);

  let id = temp.split('"')[0];

  return id;
}

function getState(command) {
  let temp = command.substring(command.indexOf("STATE=") + 7);

  let state = temp.split('"')[0];

  return state;
}
